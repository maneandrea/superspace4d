(* ::Package:: *)

(* ::Title:: *)
(*Superspace*)


(* ::Section::Closed:: *)
(*Beginning of package and function descriptions*)


(* ::Subsection::Closed:: *)
(*Usage messages*)


BeginPackage["Superspace4d`"];


ClearAll[\[Theta],\[Theta]b,x,\[Eta],\[Eta]b,\[Xi],\[Chi],\[Chi]b,\[Zeta],s,d,c,e,t,u,v,y];


(* Usage of functions *)
(** Wrappers **)
c::usage = "c[...] represents a fully contracted monomial where the first and last object have a single spinor index";
t::usage = "t[...] represents a trace of \!\(\*SubscriptBox[\(x\), \(\[Alpha] \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\) matrices";
d::usqge = "d[\!\(\*
StyleBox[\"x1\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"x2\",\nFontSlant->\"Italic\"]\)] represents the dot product \!\(\*
StyleBox[\"x1\",\nFontSlant->\"Italic\"]\)\[CenterDot]\!\(\*
StyleBox[\"x2\",\nFontSlant->\"Italic\"]\)";
rc::usage = "rc[...] represents a fully contracted monomial where the first and last object have a single R-symmetry index or a single spinor index";
rt::usage = "rt[...] represents a trace of \!\(\*SubscriptBox[\(x\), \(\[Alpha] \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\) matrices or \!\(\*SubscriptBox[\(\[Theta]\), \(a\[Alpha]\)]\) Grassmann variables";
e::usage = "e[...] represents the Levi-Civita tensor contracted with four x's";
\[Theta]::usage = "\[Theta][i] represents the Grassmann variable \[Theta] at point i";
\[Theta]b::usage = "\[Theta]b[i] represents the Grassmann variable \[Theta]b at point i";
x::usage = "x[i] represents the position x at point i while x[i,j] represents the difference x[i] - x[j]";
\[Eta]::usage = "\[Eta][i] represents the spin polariation \[Eta] at point i";
\[Eta]b::usage = "\[Eta]b[i] represents the spin polariation \[Eta]b at point i";
\[Xi]::usage = "\[Xi][i] represents the R-symmetry polariation \[Xi] at point i";
\[Chi]::usage = "\[Chi] is a placeholder for a temporary \[Eta], should only appear in internal computations";
\[Chi]b::usage = "\[Chi]b is a placeholder for a temporary \[Eta]b, should only appear in internal computations";
\[Zeta]::usage = "\[Zeta] is a placeholder for a temporary \[Xi], should only appear in internal computations";
(** Main functions **)
allReduce::usage = "allReduce[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] applies all reduction rules to \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) in sequence. Namely:\nfullExpand, productReduce, equal\[Theta]Reduce, chainReduce, traceReduce, epsilonReduce and productReduce.\nThen it collects the terms using collectFunction.";
allReduceExperimental::usage = "like allReduce but uses the function Simplify";
allReduceExperimentalTest::usage = "";
applyPrefactor::usage = "applyPrefactor[{\[CapitalDelta]1,j1,jb1}, {\[CapitalDelta]2,j2,jb2}, \!\(\*StyleBox[\"tpf\",\nFontSlant->\"Italic\"]\)] applies the three-point function prefactor to \!\(\*StyleBox[\"tpf\",\nFontSlant->\"Italic\"]\) in order to map from the Osborn representation to the position space representation";
chainReduce::usage = "applies the reduction rules to chains c[__]";
compare::usage = "compare[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"vars\",\nFontSlant->\"Italic\"]\)[, \!\(\*StyleBox[\"options\",\nFontSlant->\"Italic\"]\)]] equates an expression \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) to zero and yields a solution for the symbols \!\(\*StyleBox[\"vars\",\nFontSlant->\"Italic\"]\) which were specified as variables";
complexConj::usage = "applies complex conjugation to an expression";
epsilonReduce::usage = "applies the reduction rules to epsilon contractions e[__]";
equal\[Theta]Reduce::usage = "applies the reduction rules to expressions containing two identical \[Theta]'s";
format::usage = "toggles between the formatted and the unformatted output";
fullExpand::usage = "fully expands including also ** operator";
productLinearity::usage = "fully expands including also the c,t,d and ** operators and splits the \[CircleTimes] inside chains and traces";
productReduce::usage = "applies the distributive property and pulls out bosonic factors";
productReorder::usage = "reorders canonically terms in a product";
traceReduce::usage = "applies the reduction rules to trace expressions t[___]";
xdx::usage = "xdx[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), s, x] takes a directional derivative \!\(\*SuperscriptBox[\(s\), \(\[Mu]\)]\) \!\(\*SubsuperscriptBox[\(\[PartialD]\), \(\[Mu]\), \(x\)]\) of expr";
\[Eta]d\[Eta]::usage = "\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Chi], \[Eta]] takes a directional derivative \!\(\*SuperscriptBox[\(\[Chi]\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta], \[Alpha]\)]\) or \!\(\*SubscriptBox[OverscriptBox[\(\[Chi]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
\[Eta]d\[Theta]::usage = "\[Eta]d\[Theta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Chi], \[Theta]] takes a directional derivative \!\(\*SuperscriptBox[\(\[Chi]\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta], \[Alpha]\)]\) or \!\(\*SubscriptBox[OverscriptBox[\(\[Chi]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
\[Theta]d\[Eta]::usage = "\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Theta], \[Eta]] takes a directional derivative \!\(\*SuperscriptBox[\(\[Theta]\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta], \[Alpha]\)]\) or \!\(\*SubscriptBox[OverscriptBox[\(\[Theta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
\[Theta]d\[Theta]::usage = "\[Theta]d\[Theta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Theta]', \[Theta]] takes a directional derivative \[Theta]\!\(\*SuperscriptBox[\('\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta], \[Alpha]\)]\) or \!\(\*SubscriptBox[OverscriptBox[\(\[Theta]'\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\)\!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
dxdx::usage = "dxdx[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), x1, x2] takes a derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(x1\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(x2\)]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
dxsq::usage = "dxsq[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), x] is an alias for dxdx[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), x, x]";
d\[Eta]d\[Eta]::usage = "d\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Eta]1, \[Eta]2] takes a derivative \!\(\*SubsuperscriptBox[\(\[PartialD]\), \(\[Eta]1\), \(\[Alpha]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Eta]2, \[Alpha]\)]\) or \!\(\*SubscriptBox[\(\[PartialD]\), \(\*OverscriptBox[\(\[Eta]1\), \(_\)], \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\) \!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Eta]1\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
d\[Theta]d\[Theta]::usage = "d\[Theta]d\[Theta][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \[Theta]1, \[Theta]2] takes a derivative \!\(\*SubsuperscriptBox[\(\[PartialD]\), \(\[Theta]1\), \(\[Alpha]\)]\) \!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]2, \[Alpha]\)]\) or \!\(\*SubscriptBox[\(\[PartialD]\), \(\*OverscriptBox[\(\[Theta]1\), \(_\)], \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\) \!\(\*SubsuperscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]1\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
compositeD::usage = "compositeD[\!\(\*StyleBox[\"op\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] = compositeD[\!\(\*StyleBox[\"op\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] takes a derivative of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) specified by the operator \!\(\*StyleBox[\"op\",\nFontSlant->\"Italic\"]\) where a derivative w.r.t. X is denoted as dX (X=\[Theta],\[Eta],...)";
CircleTimes::usage = "\[Eta] \[CircleTimes] \[Eta]b represents the matrix obtained by the outer product of \[Eta] and \[Eta]b, similarly for \[Theta]";
fPower::usage = "fPower[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"nmax\",\nFontSlant->\"Italic\"]\):NMAX] raises \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) to the power \!\(\*StyleBox[\"p\",\nFontSlant->\"Italic\"]\).\!\(\*StyleBox[\" \",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\"nmax\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontSlant->\"Italic\"]\)is used only if \!\(\*StyleBox[\"p\",\nFontSlant->\"Italic\"]\) is not a positive integer and it determines the maximal order to which to expand (default=10)";
fTaylor::usage = "fTaylor[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontSlant->\"Italic\"]\)] computes the Taylor expansion of \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] around \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) = 0 up to order \!\(\*StyleBox[\"n\",\nFontSlant->\"Italic\"]\)";
opD::usage = "opD[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the operator \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\(\[Alpha]\)\(\\\ \)\)]\)\!\(\*SubscriptBox[\(D\), \(\[Alpha]\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opDb::usage = "opDb[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the operator \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], \(\*OverscriptBox[\(\[Alpha]\), \(.\)]\(\\\ \)\)]\)\!\(\*SubscriptBox[OverscriptBox[\(D\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQ::usage = "opQ[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the operator \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\(\[Alpha]\)\(\\\ \)\)]\)\!\(\*SubscriptBox[\(Q\), \(\[Alpha]\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQb::usage = "opQb[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the operator \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], \(\*OverscriptBox[\(\[Alpha]\), \(.\)]\(\\\ \)\)]\)\!\(\*SubscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQp::usage = "opQp[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace Poincar\[EAcute] supercharge \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\[Alpha]\)]\) \!\(\*SubsuperscriptBox[\(Q\), \(\[Alpha]\), \(+\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQm::usage = "opQm[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace Poincar\[EAcute] supercharge \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\[Alpha]\)]\) \!\(\*SubsuperscriptBox[\(Q\), \(\[Alpha]\), \(-\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQbp::usage = "opQbp[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace Poincar\[EAcute] supercharge \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*SubsuperscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)], \(+\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opQbm::usage = "opQbm[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace Poincar\[EAcute] supercharge \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*SubsuperscriptBox[OverscriptBox[\(Q\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)], \(-\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opSp::usage = "opSp[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace special conformal supercharge \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\[Alpha]\)]\) \!\(\*SubsuperscriptBox[\(S\), \(\[Alpha]\), \(+\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opSm::usage = "opSm[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"eta\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace special conformal supercharge \!\(\*SuperscriptBox[StyleBox[\"eta\",\nFontSlant->\"Italic\"], \(\[Alpha]\)]\) \!\(\*SubsuperscriptBox[\(S\), \(\[Alpha]\), \(-\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opSbp::usage = "opSbp[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace special conformal supercharge \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*SubsuperscriptBox[OverscriptBox[\(S\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)], \(+\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
opSbm::usage = "opSbm[\!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"etab\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] implements the harmonic superspace special conformal supercharge \!\(\*SuperscriptBox[StyleBox[\"etab\",\nFontSlant->\"Italic\"], OverscriptBox[\(\[Alpha]\), \(.\)]]\) \!\(\*SubsuperscriptBox[OverscriptBox[\(S\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)], \(-\)]\) acting on point \!\(\*StyleBox[\"point\",\nFontSlant->\"Italic\"]\) in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\).";
g::usage = "g[\!\(\*StyleBox[\"i\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"j\",\nFontSlant->\"Italic\"]\)] is the super-propagator \!\(\*FractionBox[SubscriptBox[OverscriptBox[\"y\", \"^\"], StyleBox[\"ij\",\nFontSlant->\"Italic\"]], SubsuperscriptBox[\"x\", StyleBox[\"ij\",\nFontSlant->\"Italic\"], \"2\"]]\) with \!\(\*SubscriptBox[OverscriptBox[\(y\), \(^\)], StyleBox[\"ij\",\nFontSlant->\"Italic\"]]\) = \!\(\*SubscriptBox[\(y\), StyleBox[\"ij\",\nFontSlant->\"Italic\"]]\) - 4\[ImaginaryI] \!\(\*SubscriptBox[\(\[Theta]\), StyleBox[\"ij\",\nFontSlant->\"Italic\"]]\)\!\(\*SubsuperscriptBox[\(x\), StyleBox[\"ij\",\nFontSlant->\"Italic\"], \(-1\)]\)\!\(\*SubscriptBox[OverscriptBox[\(\[Theta]\), \(_\)], StyleBox[\"ij\",\nFontSlant->\"Italic\"]]\)";
opQbpTerm1::usage = "see general help message by typing ?opXTermY";
opQbpTerm2::usage = "see general help message by typing ?opXTermY";
opQpTerm1::usage = "see general help message by typing ?opXTermY";
opQpTerm2::usage = "see general help message by typing ?opXTermY";
opSbmTerm1::usage = "see general help message by typing ?opXTermY";
opSbmTerm2::usage = "see general help message by typing ?opXTermY";
opSmTerm1::usage = "see general help message by typing ?opXTermY";
opSmTerm2::usage = "see general help message by typing ?opXTermY";
opSpTerm1::usage = "see general help message by typing ?opXTermY";
opSpTerm2::usage = "see general help message by typing ?opXTermY";
opSpTerm3::usage = "see general help message by typing ?opXTermY";
opSpTerm4::usage = "see general help message by typing ?opXTermY";
opSbpTerm1::usage = "see general help message by typing ?opXTermY";
opSbpTerm2::usage = "see general help message by typing ?opXTermY";
opSbpTerm3::usage = "see general help message by typing ?opXTermY";
opSbpTerm4::usage = "see general help message by typing ?opXTermY";
opXTermY::usage = "This symbol only provides a usage message. The usage of \*StyleBox[\(op\!\(\*StyleBox[\"X\",\nFontSlant->\"Italic\"]\)TermY\)] is the same as the corresponding op\!\(\*StyleBox[\"X\",\nFontSlant->\"Italic\"]\). \nopQpTerm1 : -y \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\)\nopQpTerm2 : 2 \[ImaginaryI] \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)\!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\)\nopQbpTerm1,2 : conjugates\nopSpTerm1 : 2 \!\(\*SuperscriptBox[\(\[Theta]\), \(2\)]\) \[Eta]\!\(\*SubscriptBox[\(\[PartialD]\), \(\[Theta]\)]\)\nopSpTerm2 : 4y \[Theta]\[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(y\)]\)\nopSpTerm3 : \[ImaginaryI] y \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\)x\[Eta]\nopSpTerm4 : 2 \[Theta]\!\(\*SubscriptBox[\(\[PartialD]\), \(x\)]\)x\[Eta]\nopSbpTerm1,2,3,4 : conjugates\nopSmTerm1 : -4 \[Theta]\[Eta] \!\(\*SubscriptBox[\(\[PartialD]\), \(y\)]\)\nopSmTerm2 : -\[ImaginaryI] \!\(\*SubscriptBox[\(\[PartialD]\), OverscriptBox[\(\[Theta]\), \(_\)]]\)x\[Eta]\nopSbmTerm1,2 : conjugates";
nonSUSY3pf::usage="nonSUSY3pf[{\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\)}, {{\!\(\*StyleBox[\"j1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"j3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontSlant->\"Italic\"]\)}}] returns a non-supersymmetric three-point function of the operators with conformal dimensions \!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\) and spins (\!\(\*StyleBox[\"j1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb1\",\nFontSlant->\"Italic\"]\)), (\!\(\*StyleBox[\"j2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb2\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"j3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb3\",\nFontSlant->\"Italic\"]\)). This is obtained by translating to user notation the output of CFTs4D`n3CorrelationFunction";
nonSUSY2pf::usage="nonSUSY2pf[\!\(\*StyleBox[\"\[CapitalDelta]\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontSlant->\"Italic\"]\)] returns the non-supersymmetric two-point function of an operator with conformal dimension \!\(\*StyleBox[\"\[CapitalDelta]\",\nFontSlant->\"Italic\"]\) and spin (\!\(\*StyleBox[\"j\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"jb\",\nFontSlant->\"Italic\"]\))";
(** Auxiliary functions **)
collectFunction::usage = "collectFunction[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] writes \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) collecting various terms for better readability";
bosonQ::usage = "bosonQ[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] is true if the fermion number of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) is even";
fermionQ::usage = "fermionQ[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] is true if the fermion number of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) is odd";
FN::usage = "FN[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] is the fermion number of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)";
glue::usage = "glue[\!\(\*StyleBox[\"a\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontSlant->\"Italic\"]\), ...] makes a symbol concatenating \!\(\*StyleBox[\"ab\",\nFontSlant->\"Italic\"]\)...";
niceRules::usage = "niceRules[\!\(\*StyleBox[\"vars\",\nFontSlant->\"Italic\"]\)] is a replacement that collects \!\(\*StyleBox[\"vars\",\nFontSlant->\"Italic\"]\) on the right hand sides of Rules";
ordered\[Theta]List::usage = "ordered\[Theta]List[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] retuns a list of all Grassmann variables in \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) in the order in which they appear";
Rex::usage = "Rex[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"rule\",\nFontSlant->\"Italic\"]\)] == Rex[\!\(\*StyleBox[\"rule\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] repeatedly applies \!\(\*StyleBox[\"rule\",\nFontSlant->\"Italic\"]\) to \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) expanding the result each time: Expand[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)]/.\!\(\*StyleBox[\"rule\",\nFontSlant->\"Italic\"]\)";
sq::usage = "sq[x[i__]] equals d[x[i],x[i]] while sq[\[Theta][i__]] = c[\[Theta][i],\[Theta][i]] (same for \[Theta]b).";
fromEpsilonToTrace::usage = "turns all \[Epsilon](...) in an expression into combinations of traces t(...) and scalar products \!\(\*SubscriptBox[\(x\), \(i\)]\)\[CenterDot]\!\(\*SubscriptBox[\(x\), \(j\)]\).";
splitij::usage = "splitij[\!\(\*StyleBox[\"string\",\nFontSlant->\"Italic\"]\)] returns a replacement rule splitting \!\(\*SubscriptBox[\(X\), \(ij\)]\) to \!\(\*SubscriptBox[\(X\), \(i\)]\)-\!\(\*SubscriptBox[\(X\), \(j\)]\) where X is any of the symbols \[Theta],x or y that are contained in \!\(\*StyleBox[\"string\",\nFontSlant->\"Italic\"]\), e.g. splitij[\"\[Theta]y\"] splits only \[Theta], \[Theta]b and y";
toCF::usage = "toCF[\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"\[CapitalDelta]4\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] rewrites \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) in conformal frame using the notation of CFTs4D, where \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) is a four-point function with external dimensions \!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]4\",\nFontSlant->\"Italic\"]\).";
kinPref::usage = "kinPref[\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[CapitalDelta]4\",\nFontSlant->\"Italic\"]\)] is the kinematic prefactor of the four-point function of scalars with the given conformal dimensions while kinPref[{\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"l1\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"lb1\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"l2\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"lb2\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"\[CapitalDelta]3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"l3\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"lb3\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"\[CapitalDelta]4\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"l4\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"lb4\",\nFontSlant->\"Italic\"]\)}] is the kinematic prefactor of the four-point function of operators with given spin and conformal dimension";
(** Internal functions **)
alternateMap::usage = "alternateMap[{\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\),\!\(\*StyleBox[\"g\",\nFontSlant->\"Italic\"]\)}, \!\(\*StyleBox[\"list\",\nFontSlant->\"Italic\"]\)] maps \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\) and \!\(\*StyleBox[\"g\",\nFontSlant->\"Italic\"]\) to \!\(\*StyleBox[\"list\",\nFontSlant->\"Italic\"]\) alternating {\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)[1], \!\(\*StyleBox[\"g\",\nFontSlant->\"Italic\"]\)[2], ...}";
bar::usage = "bar[\!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)] toggles the barred and unbared version of \!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)";
barredQ::usage = "barredQ[\!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)] returns True if \!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\) is barred (like \!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\) or \!\(\*OverscriptBox[\(\[Eta]\), \(_\)]\))";
unbarredQ::usage = "unbarredQ[\!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)] is the negation of barredQ[\!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)]";
constQ::usage = "constQ[\!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\)] returns True if \!\(\*StyleBox[\"symbol\",\nFontSlant->\"Italic\"]\) is to be considered a constant. You can define new constants with UpValues this way.";
dPower::usage = "dPower[\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"l\",\nFontSlant->\"Italic\"]\)] returns the function \!\(\*StyleBox[\"l\",\nFontSlant->\"Italic\"]\) #1^(\!\(\*StyleBox[\"l\",\nFontSlant->\"Italic\"]\)-1) \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)";
fSignature::usage = "fSignature[\!\(\*StyleBox[\"list\",\nFontSlant->\"Italic\"]\)] determines the signature of the permutation that takes \!\(\*StyleBox[\"list\",\nFontSlant->\"Italic\"]\) to canonical order accounting only for Grassmann variables";
gradedLeibniz::usage = "gradedLeibniz[\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)] returns a function that makes \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\) act on products using the graded Leibniz rule";
Leibniz::usage = "gradedLeibniz[\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)] returns a function that makes \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\) act on products using the Leibniz rule";
linearity::usage = "linearity[\!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\)] returns a function that makes \!\(\*StyleBox[\"f\",\nFontSlant->\"Italic\"]\) act on linear combination using the linearity property";
hasIndicesDown::usage = "Returns True if the x after the chain or trace given has indices down and False otherwise";
make\[Theta]sUnique::usage = "make\[Theta]sUnique[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] returns {\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)', \!\(\*StyleBox[\"rules\",\nFontSlant->\"Italic\"]\)} where \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)' is a modification of \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",\nFontSlant->\"Italic\"]\)where all the \[Theta]'s are unique and \!\(\*StyleBox[\"rules\",\nFontSlant->\"Italic\"]\) are a list of replacement rules to go back to the original expression";
tryOverAndOver::usage = "tryOverAndOver[\!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\)] attempts evaluating \!\(\*StyleBox[\"expr\",\nFontSlant->\"Italic\"]\) up to MAXTRIALS times until the exceptions Thread::tdlen and Power::infy stop appearing";
allowedF::usage = "List containing the Symbols which are allowed as symbolic functions when acting with differential operators";


(* ::Subsection::Closed:: *)
(*Formatting*)


(* Without checking BoxForm`UseIcons, printing definitions with ? or ?? will generate an infinite recursion. *)
safeInterpretation[formatted_, raw_] := If[BoxForm`UseIcons, 
	Interpretation[formatted,raw],
	formatted /. Row[x_] :> Infix[x,""] /. Infix[{x_},""] :> x (* Text shown in the output of ? and ?? *)];


(* Some formatting. This affects only the way symbols are presented as output. *)
(** If you copy-paste the formatted output, Mathematica will still interpret it as the underlying expression. **)
(** If you copy-paste the formatted output with Copy As > Input Text, Mathematica will paste it as a raw expression. **)
(** Call the function format[] to toggle formatting. **)
formatting=False;
format[]:=Module[{},
	If[Not[formatting],
		Format[\[ScriptCapitalC][n_], StandardForm] :=      safeInterpretation[Subscript[\[ScriptCapitalC], n],\[ScriptCapitalC][n]];
		Format[x[i__], StandardForm] :=     safeInterpretation[Subscript[x,Row[{i}]],x[i]];
		Format[y[i__], StandardForm] :=     safeInterpretation[Subscript[y,Row[{i}]],y[i]];
		Format[s[i_], StandardForm] :=      safeInterpretation[Style[Subscript[s,i],Red],s[i]];
		Format[\[Theta][i__], StandardForm] :=     safeInterpretation[Subscript[\[Theta],Row[{i}]],\[Theta][i]];
		Format[\[Theta]b[i__], StandardForm] :=    safeInterpretation[Subscript[OverBar[\[Theta]],Row[{i}]],\[Theta]b[i]];
		Format[\[Eta][i_], StandardForm] :=      safeInterpretation[Subscript[\[Eta],i],\[Eta][i]];
		Format[\[Xi][i_], StandardForm] :=      safeInterpretation[Subscript[\[Xi],i],\[Xi][i]];
		Format[\[Chi][i_], StandardForm] :=      safeInterpretation[Style[Subscript[\[Chi],i],Red],\[Chi][i]];
		Format[\[Zeta][i_], StandardForm] :=      safeInterpretation[Style[Subscript[\[Zeta],i],Red],\[Zeta][i]];
		Format[\[Eta]b[i_], StandardForm] :=     safeInterpretation[Subscript[OverBar[\[Eta]],i],\[Eta]b[i]];
		Format[\[Chi]b[i_], StandardForm] :=     safeInterpretation[Style[Subscript[OverBar[\[Chi]],i],Red],\[Chi]b[i]];
		(**)
		Format[dx[i__], StandardForm] :=    safeInterpretation[Subscript["\[PartialD]", Subscript[x,Row[{i}]]],dx[i]];
		Format[d\[Theta][i_], StandardForm] :=     safeInterpretation[Subscript["\[PartialD]", Subscript[\[Theta],i]],d\[Theta][i]];
		Format[d\[Theta]b[i_], StandardForm] :=    safeInterpretation[Subscript["\[PartialD]", Subscript[OverBar[\[Theta]],i]],d\[Theta]b[i]];
		Format[d\[Eta][i_], StandardForm] :=     safeInterpretation[Subscript["\[PartialD]", Subscript[\[Eta],i]],d\[Eta][i]];
		Format[d\[Eta]b[i_], StandardForm] :=    safeInterpretation[Subscript["\[PartialD]", Subscript[OverBar[\[Eta]],i]],d\[Eta]b[i]];
		(**)
		Format[c[xx__], StandardForm] :=    safeInterpretation[Row[{xx}],c[xx]];
		Format[t[xx__], StandardForm] :=    safeInterpretation[Row[{"tr(",xx,")"}],t[xx]];
		Format[rc[xx__], StandardForm] :=   safeInterpretation[Row[{xx}],rc[xx]];
		Format[rt[xx__], StandardForm] :=   safeInterpretation[Row[{"Tr(",xx,")"}],rt[xx]];
		Format[e[xx__], StandardForm] :=    safeInterpretation[Row[{"\[Epsilon](",xx,")"}],e[xx]];
		Format[d[x1_,x1_], StandardForm] := safeInterpretation[Superscript[x1,2],d[x1,x1]];
		Format[d[x1_,x2_], StandardForm] := safeInterpretation[CenterDot[x1,x2],d[x1,x2]];
		(**)
		Format[zb, StandardForm] :=         safeInterpretation[OverBar[z],zb];
		Format[CFTs4D`CF4pt[{\[CapitalDelta]__},{ls__},{lb__},{qs__},{qbs__},g_], StandardForm] := safeInterpretation[DisplayForm@RowBox[{"[",Column[{Row[{qs},","],Row[{qbs},","]}],"]"}] g,CFTs4D`CF4pt[{\[CapitalDelta]},{ls},{lb},{qs},{qbs},g]];
		formatting=True;
		,
		Format[\[ScriptCapitalC][n_], StandardForm]=.;
		Format[x[i__], StandardForm]=.;
		Format[y[i__], StandardForm]=.;
		Format[s[i_], StandardForm]=.;
		Format[\[Theta][i__], StandardForm]=.;
		Format[\[Theta]b[i__], StandardForm]=.;
		Format[\[Eta][i_], StandardForm]=.;
		Format[\[Xi][i_], StandardForm]=.;
		Format[\[Zeta][i_], StandardForm]=.;
		Format[\[Eta]b[i_], StandardForm]=.;
		Format[\[Chi][i_], StandardForm]=.;
		Format[\[Chi]b[i_], StandardForm]=.;
		Format[dx[i__], StandardForm]=.;
		Format[d\[Theta][i_], StandardForm]=.;
		Format[d\[Theta]b[i_], StandardForm]=.;
		Format[d\[Eta][i_], StandardForm]=.;
		Format[d\[Eta]b[i_], StandardForm]=.;
		Format[c[xx__], StandardForm]=.;
		Format[t[xx__], StandardForm]=.;
		Format[rc[xx__], StandardForm]=.;
		Format[rt[xx__], StandardForm]=.;
		Format[e[xx__], StandardForm]=.;
		Format[d[x1_,x1_], StandardForm]=.;
		Format[d[x1_,x2_], StandardForm]=.;
		Format[CFTs4D`CF4pt[{\[CapitalDelta]__},{ls__},{lb__},{qs__},{qbs__},g_], StandardForm]=.;
		Format[zb, StandardForm]=.;
		formatting=False;
	]
];
format[];


(* ::Subsection::Closed:: *)
(*Input*)


fromString[expr_] := expr /. X_Symbol /; Context[X] =!= "System`" :> fromStringAux[X];


fromString::parse="could not parse `1`";


alternatives\[Theta]\[Eta] = "\[Theta]"|"\[Eta]"|"\[Theta]b"|"\[Eta]b"|"d\[Theta]"|"d\[Eta]"|"d\[Theta]b"|"d\[Eta]b";


fromStringAux[symbol_] := Module[{str},
	str = ToString[symbol];
	Which[
		StringMatchQ[str,(alternatives\[Theta]\[Eta])~~DigitCharacter..~~(("x"|"dx")~~DigitCharacter..)...~~(alternatives\[Theta]\[Eta])~~DigitCharacter..],
			fromStringAuxCTD[str, c],
		StringMatchQ[str,"tr"~~("x"~~DigitCharacter..)...],
			fromStringAuxCTD[StringDrop[str,2], t],
		StringMatchQ[str,"\[Epsilon]"~~("x"~~DigitCharacter..)...],
			fromStringAuxCTD[StringDrop[str,1], e],
		StringMatchQ[str,(("x"|"dx")~~DigitCharacter..)~~(("x"|"dx")~~DigitCharacter..)],
			fromStringAuxCTD[str, d],
		StringMatchQ[str,(("x"|"dx")~~DigitCharacter..)~~"sq"],
			fromStringAuxSQ[str, d],
		StringMatchQ[str,(("\[Theta]"|"\[Theta]b"|"d\[Theta]"|"d\[Theta]b")~~DigitCharacter..)~~"sq"],
			fromStringAuxSQ[str, c],
		True,If[constQ[symbol],symbol,Message[fromString::parse,str]]
	]
];

xij\[Theta]ijrule = {"x["~~a:DigitCharacter~~b:DigitCharacter~~"]"/;a=!=b :> "x["<>a<>","<>b<>"]",
		"\[Theta]["~~a:DigitCharacter~~b:DigitCharacter~~"]"/;a=!=b :> "\[Theta]["<>a<>","<>b<>"]",
		"\[Theta]b["~~a:DigitCharacter~~b:DigitCharacter~~"]"/;a=!=b :> "\[Theta]b["<>a<>","<>b<>"]"
};

fromStringAuxCTD[symbolstr_,ctd_] := ctd@@(ToExpression[
	StringReplace[
		StringReplace[
			#,
			(A:DigitCharacter..):>"["<>A<>"]"
		],
		xij\[Theta]ijrule
	]
]&/@StringSplit[
	symbolstr,
	RegularExpression["(?<=\\d)(?!\\d)"]
]);
fromStringAuxSQ[symbolstr_,cd_] := ToExpression[
	StringReplace[
		StringReplace[
			symbolstr,
			(h:"\[Theta]"|"\[Theta]b"|"x"|"d\[Theta]"|"d\[Theta]b"|"dx")~~(A:DigitCharacter..)~~"sq":>ToString[cd]<>"["<>h<>"["<>A<>"],"<>h<>"["<>A<>"]]"
		],
		xij\[Theta]ijrule
	]
]


(* ::Section::Closed:: *)
(*Initial definitions*)


(* ::Subsection::Closed:: *)
(*Constants*)


(* This determines whether a quantity is a constant or (part of) a tensor structure *)
constQ[expr_] := FreeQ[expr, \[Eta]|\[Eta]b|\[Theta]|\[Theta]b|x|c|d|e|t];


(* This takes out squares and dot products from chains and traces *)
takeOutable[d[x__]] := True;
takeOutable[Power[x_,pow_]] := True;
takeOutable[x_] := False;


(* Allowed symbols for symbolic functions *)
allowedF = {f,F,\[ScriptF],\[ScriptG],\[ScriptCapitalF],\[ScriptCapitalG],\[Phi],\[CapitalPhi],\[CurlyPhi],A,B,\[ScriptCapitalA],\[ScriptCapitalB]};
altF := Alternatives@@allowedF;


(* ::Subsection::Closed:: *)
(*Fermion number*)


(* Determines the fermion number (1 or 0) of a quantity *)
FN[x_?NumericQ]:=0;
FN[x_]/;FreeQ[x,\[Theta]|\[Theta]b] = 0;
FN[x_Times]:=Mod[Plus@@(FN/@List@@x),2];
FN[CircleTimes[a_,b_]]:=Mod[FN[a]+FN[b],2];
FN[x_NonCommutativeMultiply]:=Mod[Plus@@(FN/@List@@x),2];
FN[a_,b__]:=Mod[FN[a]+FN[b],2];
FN[]:=0;
FN[a_^b_]:=0;
FN[a_+b_]:=If[FN[a]==FN[b],FN[a],Message[FN::FNfail,a+b];Inactive[FN][a+b],Message[FN::FNfail,a+b];Inactive[FN][a+b]];
FN[x_c]:=Mod[Plus@@(FN/@List@@x),2];
FN[x_e]:=0;
FN[x_t]:=0;
FN[x_d]:=0;
FN[x[i__]]:=0;
FN[\[Eta]b[i_]]:=0;
FN[\[Eta][i_]]:=0;
FN[\[Theta][i__]]:=1;
FN[\[Theta]b[i__]]:=1;
FN[x_Symbol]:=0


fermionQ[x_] := (FN[x] == 1);
bosonQ[x_]   := (If[FN[x] == 0,True,False,Message[FN::FNfail,x];True]);


FN::FNfail = "The expression `1` has undefined fermion number.";


(* ::Subsection::Closed:: *)
(*NonCommutativeMultiply*)


MAXITERATIONS=100;


(* Applies the distributive property and pulls out bosonic factors *)
productReduce[expr_] := FixedPoint[productReduceAux, expr,MAXITERATIONS];
productReduceAux[expr_] := expr /. x_NonCommutativeMultiply :> Module[{temp,$wait},
	temp = Distribute[ExpandAll[x]] /. NonCommutativeMultiply -> $wait //. $wait[a___,b_*c_,d___] :> $wait[a,b,c,d] /. $wait[a__] :> Flatten[$wait[a],\[Infinity],$wait];
	temp /. $wait[a__] :> Times@@(Select[{a},bosonQ]) $wait@@(Select[{a},fermionQ]) /. {
		$wait[] -> 1, $wait[a_] :> a, $wait[a_,b__] :> NonCommutativeMultiply[a,b]
	}
];


(* Expands out the ** product, unlike the previous function, this does not care about pulling out bosonic factors *)
fullExpand[expr_] := Expand[ExpandAll[expr] //. {
	___**0**___ :> 0,
	a___**(b_ + k_)**f___ :> a**b**f + a**k**f,
	a___**(b_ k_?constQ)**f___ :> k a**b**f,
	a___**(b_ k_)**f___ :> a**b**k**f
}];


fSignature[perm_] := Signature[Select[perm,fermionQ]]


(* Reorders a product canonically *)
productReorder[expr_] := expr /. x_NonCommutativeMultiply :> Module[{temp,$wait},
	temp = x /. NonCommutativeMultiply -> $wait;
	temp /. $wait[a__] :> fSignature[{a}]NonCommutativeMultiply@@Sort[{a}]
];


(* ::Subsection::Closed:: *)
(*Useful auxiliary functions*)


glue[x__]:= ToExpression[StringTrim[StringJoin@@(ToString/@{x})," "]];


niceRules[vars_:\[ScriptCapitalC][_]] = {Rule[a_,b_] :> Rule[a, Collect[b,vars,Factor]]};


(* Apply a replacement rule repeatedly expanding the expression every time *)
Rex[expr_,rule_]:=FixedPoint[Expand[#]/.rule&,expr,MAXITERATIONS];
Rex[rule_]:=Rex[#,rule]&;


sq[x[i__]] := d[x[i],x[i]];
sq[\[Theta][i__]] := c[\[Theta][i],\[Theta][i]];
sq[\[Theta]b[i__]] := c[\[Theta]b[i],\[Theta]b[i]];
bar[\[Theta][i__]] := \[Theta]b[i];
bar[\[Theta]b[i__]] := \[Theta][i];
bar[\[Eta][i_]] := \[Eta]b[i];
bar[\[Eta]b[i_]] := \[Eta][i];
bar[x[i__]] := x[i];


barredQ[\[Eta][i_]] := False;
barredQ[\[Eta]b[i_]] := True;
barredQ[\[Theta][i__]] := False;
barredQ[\[Theta]b[i__]] := True;
unbarredQ[e_] := Not[barredQ[e]];


(* Maps two functions alternating alternateMap[{f1,f2},{1,2,3,4}] = {f1[1],f2[2],f1[3],f2[4]} *)
alternateMap[{fodd_,feven_},list_] := Table[
	({fodd, feven}[[Mod[nn,2,1]]])[list[[nn]]]
,{nn, 1, Length[list]}];


(* Gives a list of all the Grassmann variables in the order in which they appear *)
ordered\[Theta]List::NotAProduct = "The expression `1` is not a product of terms.";
ordered\[Theta]List[mon_Times|mon_NonCommutativeMultiply] := Module[{factors},
	factors = List@@mon //. {
		{l___,a_ b_, r___} :> {l,a,b,r},
		{l___,a_**b_, r___} :> {l,a,b,r}
	};
	Join@@(ordered\[Theta]List/@factors)
];
ordered\[Theta]List[c[xx__]] := Select[{xx},fermionQ]
ordered\[Theta]List[d[xx__]] := {};
ordered\[Theta]List[e[xx__]] := {};
ordered\[Theta]List[t[xx___]] := {};
ordered\[Theta]List[expr_?constQ] := {};
ordered\[Theta]List[b_^p_?IntegerQ]/;p>0 := Flatten[Table[ordered\[Theta]List[b], {\[ScriptT],1,p}]];
ordered\[Theta]List[b_^p_] := {};
ordered\[Theta]List[expr_Plus] := Message[ordered\[Theta]List::NotAProduct, expr];


(* Gives zero if more than two identical \[Theta]'s appear in the same product *)
tooMany\[Theta]Q[list_] := If[
	Or@@(#>2&/@Tally[list][[All,2]]),
	0,1
];


splitij[string_] := Join[
	If[StringContainsQ[string,"\[Theta]"],{\[Theta][i_,j_] :> \[Theta][i] - \[Theta][j], \[Theta]b[i_,j_] :> \[Theta]b[i] - \[Theta]b[j]},{}],
	If[StringContainsQ[string,"x"],{x[i_,j_] :> x[i] - x[j]},{}],
	If[StringContainsQ[string,"y"],{y[i_,j_] :> y[i] - y[j]},{}]
]


(* ::Subsection::Closed:: *)
(*Complex conjugation*)


(* Takes the complex conjugate of an expression *)
complexConj[expr_] := expr /. {
	Complex[a_,b_] :> Complex[a, -b],
	c[seq__] :> c@@Reverse[bar/@{seq}],
	t[seq__] :> t@@RotateLeft[{seq}]
};


(* ::Section::Closed:: *)
(*Reduction rules*)


(* ::Subsection::Closed:: *)
(*Preliminary definitions*)


linearity[f_]:=Plus@@(Map[Function[{x},f[x,##2]],List@@#])&


(* ::Subsection::Closed:: *)
(*Chains*)


(*
Conventions: x has lower \[Alpha],\[Alpha]d indices, ~x has upper \[Alpha]d,\[Alpha] indices.
			c[\[Eta], x1, ..., x{2n-1}, \[Eta]b] = \[Eta]^\[Alpha] x1_\[Alpha]\[Alpha]d ... x{2n-1}_\[Alpha]\[Beta]d  \[Eta]b^\[Beta]d
			c[\[Eta], x1, ..., x{2n}, \[Eta]]    = \[Eta]^\[Alpha] x1_\[Alpha]\[Alpha]d ... ~x{2n}^\[Alpha]d\[Beta] \[Eta]_\[Beta]
			c[\[Eta]b, x1, ..., x{2n}, \[Eta]b]  = \[Eta]b_\[Alpha]d ~x1^\[Alpha]d\[Alpha] ... x{2n}_\[Alpha]\[Beta]d \[Eta]b^\[Beta]d
*)


chainReduce[expr_] := FixedPoint[chainReduceAux,expr,MAXITERATIONS];


chainReduceAux[expr_] := expr /. {
	(* Zeros *)
	c[\[Eta][i_],\[Eta][i_]] :> 0,
	c[\[Eta]b[i_],\[Eta]b[i_]] :> 0,

	(* Reducing x's *)
	c[\[Eta]L__,xi_,xi_,\[Eta]R__]:>-sq[xi]c[\[Eta]L,\[Eta]R],

	(* This speeds up the cases where there are many x1x2x1x2... but might slow down the pattern matching in more general cases. *)
	c[\[Eta]L__,Longest[rep:PatternSequence[xi_,xj_]..],\[Eta]R__] /; Length[{rep}]>2 :> With[
		{n=Length[{rep}]/2,\[Xi]=d[xi,xj]/(sq[xi]^(1/2)sq[xj]^(1/2))},
		sq[xi]^((n-1)/2) sq[xj]^((n-1)/2) ((-1)^(n+1) sq[xi]^(1/2)sq[xj]^(1/2) ChebyshevU[n-2,\[Xi]]c[\[Eta]L,\[Eta]R] + (-1)^(n+1) ChebyshevU[n-1,\[Xi]]c[\[Eta]L,xi,xj,\[Eta]R] )
	],

	c[\[Eta]L__,xi_,xj_,xi_,\[Eta]R__]:>-2d[xi,xj]c[\[Eta]L,xi,\[Eta]R] + sq[xi] c[\[Eta]L,xj,\[Eta]R],
	
	c[\[Eta]L__,xi_,xj_,xk_,xi_,\[Eta]R__]:>2d[xi,xj]c[\[Eta]L,xi,xk,\[Eta]R]-2d[xi,xk]c[\[Eta]L,xi,xj,\[Eta]R]-sq[xi]c[\[Eta]L,xj,xk,\[Eta]R],
	
	c[\[Eta]L_,xi_,xj_,xk_,xl_,\[Eta]R__] :> (
		- c[\[Eta]L,xi,xj,\[Eta]R]d[xk,xl] + c[\[Eta]L,xi,xk,\[Eta]R]d[xj,xl] - c[\[Eta]L,xi,xl,\[Eta]R]d[xj,xk] 
		- c[\[Eta]L,xj,xk,\[Eta]R]d[xi,xl] + c[\[Eta]L,xj,xl,\[Eta]R]d[xi,xk] - c[\[Eta]L,xk,xl,\[Eta]R]d[xi,xj]
		+ c[\[Eta]L,\[Eta]R](
			If[barredQ[\[Eta]L],I,-I] e[xi,xj,xk,xl] - d[xi,xj] d[xk,xl]+d[xi,xk] d[xj,xl]-d[xi,xl] d[xj,xk]
		)
	),
	
	(* Canonical ordering *)
	c[\[Eta]b[i_],x__,\[Eta][j_]] :> c[\[Eta][j], Sequence@@Reverse[{x}], \[Eta]b[i]],
	c[\[Theta]b[i__],x__,\[Eta][j_]] :> c[\[Eta][j], Sequence@@Reverse[{x}], \[Theta]b[i]],
	c[\[Eta]b[i_],x__,\[Theta][j__]] :> c[\[Theta][j], Sequence@@Reverse[{x}], \[Eta]b[i]],
	c[\[Theta]b[i__],x__,\[Theta][j__]] :> - c[\[Theta][j], Sequence@@Reverse[{x}], \[Theta]b[i]],
	
	c[\[Eta][i_],x___,\[Eta][j_]] /; i > j :> - c[\[Eta][j], Sequence@@Reverse[{x}], \[Eta][i]],
	c[\[Eta]b[i_],x___,\[Eta]b[j_]] /; i > j :> - c[\[Eta]b[j], Sequence@@Reverse[{x}], \[Eta]b[i]],
	
	c[\[Theta][i__],x___,\[Theta][j__]] /; i > j :>  c[\[Theta][j], Sequence@@Reverse[{x}], \[Theta][i]],
	c[\[Theta]b[i__],x___,\[Theta]b[j__]] /; i > j :>  c[\[Theta]b[j], Sequence@@Reverse[{x}], \[Theta]b[i]],
	
	c[\[Theta][i__],x___,\[Eta][j_]] :> - c[\[Eta][j], Sequence@@Reverse[{x}], \[Theta][i]],
	c[\[Theta]b[i__],x___,\[Eta]b[j_]] :> - c[\[Eta]b[j], Sequence@@Reverse[{x}], \[Theta]b[i]],
	
	(*** With R-symmetry indices ***)
	
	(* Zeros *)
	rc[\[Xi][i_],\[Xi][i_]] :> 0,
	rc[\[Eta][i_],\[Eta][i_]] :> 0,
	rc[\[Eta]b[i_],\[Eta]b[i_]] :> 0
}


(* ::Subsection::Closed:: *)
(*Reduction of identical \[Theta]'s to a square*)


(* We use this to make the \[Theta]'s unique so that we can keep track of where they are moved around, this also returns a replacement rule to go back *)
make\[Theta]sUnique[expr_] := Module[{positions, makerule, rule = {}},
	positions = Position[expr, \[Theta][__]|\[Theta]b[__]];
	makerule[theta_] := With[{unique = Head[theta][Unique["$"]]},
		AppendTo[rule,unique->theta];
		unique
	];
	{MapAt[makerule, expr, positions], rule}
]


(* This converts \[Theta]^\[Alpha] \[Theta]_\[Beta] into \[Delta]^\[Alpha]_\[Beta] \[Theta]^2 *)
(* This uses a function that is defined later *)
equal\[Theta]Reduce[expr_] /; FreeQ[expr, \[Theta]|\[Theta]b] := expr;
equal\[Theta]Reduce[expr_Plus] := linearity[equal\[Theta]Reduce][expr];
equal\[Theta]Reduce[expr_Times|expr_NonCommutativeMultiply|expr_c|expr_Power] := Module[{\[Theta]todo, expanded},
	expanded = fullExpand[expr];
	If[Head[expanded]===Plus, Return[linearity[equal\[Theta]Reduce][expanded]]];
	\[Theta]todo = Select[Tally[ordered\[Theta]List[expanded]],Last[#]>=2&];
	If[MemberQ[\[Theta]todo[[All,2]],n_/;(n>2)],Return[0]];
	\[Theta]todo = \[Theta]todo[[All,1]];
	Fold[equal\[Theta]ReduceAux,expanded,\[Theta]todo] // productReduce
];


equal\[Theta]ReduceAux[expr_,theta_]:= If[MatchQ[expr, c[theta,theta] other_],
	expr,
	Module[{ths={}, head\[Theta]=Head[theta], temp},
		temp = expr /. X_^p_/; Not[FreeQ[X,theta]] && IntegerQ[p] :> NonCommutativeMultiply@@Table[X,{\[ScriptI],1,p}] /. {
			theta :> With[{ind = Unique["i"]}, AppendTo[ths,head\[Theta][ind]];head\[Theta][ind]]
		};
		1/2 d\[Theta]d\[Theta][temp, ths[[2]], ths[[1]]]sq[theta]
	]
];


(* ::Subsection::Closed:: *)
(*Traces and dot*)


(*
Conventions: x has lower \[Alpha],\[Alpha]d indices, ~x has upper \[Alpha]d,\[Alpha] indices.
			t[x1, ..., x{2n}]    = x1_\[Alpha]\[Alpha]d ~x2^\[Alpha]d\[Beta] ... x{2n-1}_\[Beta]\[Beta]d ~x{2n}^\[Beta]d\[Alpha]
			Note: the trace starts with x_ and ends with ~x^.
			Also: t[xs__] is symmetric under only the cyclic permutations that shift by an even amount.
			Also: t[xs__] is invariant under reversal of the xs
*)


traceReduce[expr_] := FixedPoint[traceReduceAux,expr,MAXITERATIONS];


traceReduceAux[expr_] := expr /. {
	(* Reducing x's *)
	t[] :> 2,
	
	t[xL_,xR_] :> -2 d[xL,xR],
	
	t[xL__,xi_,xi_,xR__] :> -sq[xi]t[xL,xR],

	(* This speeds up the cases where there are many x1x2x1x2... but might slow down the pattern matching in more general cases. *)
	t[xL___,Longest[rep:PatternSequence[xi_,xj_]..],xR___] /; Length[{rep}]>2 :> With[
		{n=Length[{rep}]/2,\[Xi]=d[xi,xj]/(sq[xi]^(1/2)sq[xj]^(1/2))},
		sq[xi]^((n-1)/2) sq[xj]^((n-1)/2) ((-1)^(n+1) sq[xi]^(1/2)sq[xj]^(1/2) ChebyshevU[n-2,\[Xi]]t[xL,xR] + (-1)^(n+1) ChebyshevU[n-1,\[Xi]]t[xL,xi,xj,xR] )
	],

	t[xL___,xi_,xj_,xi_,xR___]:>-2d[xi,xj]t[xL,xi,xR] + sq[xi] t[xL,xj,xR],
	
	t[xL___,xi_,xj_,xk_,xi_,xR___]:>2d[xi,xj]t[xL,xi,xk,xR]-2d[xi,xk]t[xL,xi,xj,xR]-sq[xi]t[xL,xj,xk,xR],
	
	t[xi_,xj_,xk_,xl_,xR___] :> (
		- t[xi,xj,xR]d[xk,xl] + t[xi,xk,xR]d[xj,xl] - t[xi,xl,xR]d[xj,xk] 
		- t[xj,xk,xR]d[xi,xl] + t[xj,xl,xR]d[xi,xk] - t[xk,xl,xR]d[xi,xj]
		+ t[xR](
			-I e[xi,xj,xk,xl] - d[xi,xj] d[xk,xl]+d[xi,xk] d[xj,xl]-d[xi,xl] d[xj,xk]
		)
	),
	
	(*** With R-symmetry indices ***)
	
	(* Zeros *)
	rt[\[Theta][i_],\[Theta][i_]] -> 0,
	rt[\[Theta]b[i_],\[Theta]b[i_]] -> 0
};


SetAttributes[d,Orderless]


(* ::Subsection::Closed:: *)
(*Epsilon*)


epsilonReduce[expr_] := expr /. {
	e[ex__] /; Not[DuplicateFreeQ[{ex}]] :> 0,
	e[ex__] /; !OrderedQ[{ex}] :> Signature[{ex}] e@@Sort[{ex}],
	e[ex1__x] e[ex2__x] :> -Det[Table[d[xi,xj],{xi,{ex1}},{xj,{ex2}}]],
	e[ex1__x]^2 :> -Det[Table[d[xi,xj],{xi,{ex1}},{xj,{ex1}}]]};


fromEpsilonToTrace[expr_] := expr /. e[x1_,x2_,x3_,x4_] :> 1/2 I (-2 d[x1,x4] d[x2,x3]+2 d[x1,x3] d[x2,x4]-2 d[x1,x2] d[x3,x4]+t[x1,x2,x3,x4]);


(* ::Subsection::Closed:: *)
(*Linearity and \[CircleTimes]*)


(* Expands out all the operators *)
productLinearity[expr_] := FixedPoint[productLinearityAux, expr, MAXITERATIONS];


productLinearityAux[expr_] :=Expand[expr /. {
	(head:c|d|e|t|NonCommutativeMultiply)[X__] :> Distribute[head[X]]
} /. {
	x[i_,j_] /; i>j :>  - x[j,i],
	\[Theta][i_,j_] /; i>j :>  - \[Theta][j,i],
	\[Theta]b[i_,j_] /; i>j :>  - \[Theta]b[j,i],
	
	c[___,0,___] :> 0,
	d[___,0,___] :> 0,
	e[___,0,___] :> 0,
	t[___,0,___] :> 0,
	___**0**___ :> 0,
	
	c[a___,b_ k_?constQ,f___] :> k c[a,b,f],
	d[a___,b_ k_?constQ,f___] :> k d[a,b,f],
	e[a___,b_ k_?constQ,f___] :> k e[a,b,f],
	t[a___,b_ k_?constQ,f___] :> k t[a,b,f],
	a___**(b_ k_?constQ)**f___ :> k a**b**f,
	
	c[a___,b_ k_?takeOutable,f___] :> k c[a,b,f],
	e[a___,b_ k_?takeOutable,f___] :> k e[a,b,f],
	t[a___,b_ k_?takeOutable,f___] :> k t[a,b,f],
	a___**(b_ k_?takeOutable)**f___ :> k a**b**f,
	
	c[lef__,CircleTimes[a_,b_],rig__] :> c[lef,a]**c[b,rig],
	t[lef___,CircleTimes[a_,b_],rig___] :> c[b,rig,lef,a](-1)^(FN[a]*FN[b])
}]


(* ::Subsection::Closed:: *)
(*Combine all rules*)


(* Define a function for collecting the various terms (Just for presentation. It mustn't change the expression) *)
(* I write two alternatives, the more complicated one might be slower *)
(* collectFunction[expr_] := Collect[
	expr,
	NonCommutativeMultiply[__]|c[__], Collect[#,d[__]|e[__],Factor]&
]; *)
collectFunction[expr_] := Collect[
	expr $cwrapper[1] // Rex[{
		NonCommutativeMultiply[ex__]^p_. $cwrapper[a_] :> $cwrapper[a NonCommutativeMultiply[ex]^p],
		c[ex__]^p_.  $cwrapper[a_] :> $cwrapper[a c[ex]^p]}],
	$cwrapper[_], 
	Collect[# $dwrapper[1] // Rex[{
		d[ex__]^p_.  $dwrapper[a_] :> $dwrapper[a d[ex]^p],
		e[ex__]^p_.  $dwrapper[a_] :> $dwrapper[a e[ex]^p]}],$dwrapper[_],Factor]&
] /. {$cwrapper[ex_] :> ex, $dwrapper[ex_] :> ex};


(* Apply all the rules *)
allReduce[expr_] := collectFunction[
	productReorder[productReduce[epsilonReduce[traceReduce[chainReduce[equal\[Theta]Reduce[productReduce[productLinearity[expr]]]]]]]]
];


(* Here I try to implement the reduction rules as transformation functions of Simplify. *)
(* The complexity function forces it to reduce the length of each chain. *)
allReduceExperimental[expr_] := collectFunction[Simplify[expr,
	TransformationFunctions -> {
		Automatic, productLinearity, productReduce, equal\[Theta]Reduce, chainReduce, traceReduce, epsilonReduce, productReorder},
	ComplexityFunction -> ((
		100*Max[Append[Length/@Cases[#,c[__]|t[__],{0,\[Infinity]}], 0]] + 
		Length[Cases[#,e[a__]e[b__]|e[c__]^2,{0,\[Infinity]}]]
	)&)]];


(* This is only for testing the above function *)
allReduceExperimentalTest[expr_] := Module[{oldexpr, newexpr, compare, \[ScriptZ]},
	oldexpr = expr;
	newexpr = allReduceExperimental[expr];
	compare = compare[oldexpr - \[ScriptZ] newexpr, minEqns -> 6, variables -> {\[ScriptZ]}];
	If[Length[compare] == 0 || (\[ScriptZ] /. compare[[1]]) != 1,
		Echo["The simplification did not transform the expression into an equivalent one.",ToString[compare,InputForm]];,
		Echo["The simplification was successful.",ToString[compare]];
		Return[newexpr],
		Echo["The simplification did not transform the expression into an equivalent one.",ToString[compare,InputForm]];
	];
];


(* ::Section::Closed:: *)
(*Taylor expansion*)


(*Raises an expression to a positive integer power*)
fPower[expr_,p_?IntegerQ]/;p>0 := allReduce[NonCommutativeMultiply@@Table[expr,{i,1,p}]];
fPower[expr_,0]:=1;
(*Raises an expression to a real power*)
NMAXPOWER=10;
fPower[expr_,p_,nmax_:NMAXPOWER] := Module[{around, sum = 0, k = 0, toadd},
	around = expr /. {\[Theta][__]->0, \[Theta]b[__]->0} //. (NonCommutativeMultiply|c|t|d|CircleTimes)[___,0,___] -> 0;
	While[k<nmax,
		toadd = Binomial[p,k] around^(-k)fPower[(expr-around),k];
		k++;
		If[toadd === 0,
			Break[],			
			sum += toadd
		];
	];
	around^p sum
]
(*Computes the Taylor expansion of f[expr] around expr = 0.*)
fTaylor[expr_,f_,n_] := Sum[SeriesCoefficient[f[x],{x,0,k}] fPower[expr,k],{k,0,n}]


(* ::Section::Closed:: *)
(*Memoization*)


(* ::Subsection::Closed:: *)
(*Acts on lists*)


(* Returns f[expr] and saves the rule for f[expr] *)
memoizeAndEvaluate[f_][expr__]:=Module[{set},
    set[Inactivate[f[expr],Except[Function]],
        f[expr]
        ]/.set[LHS_,RHS_]:>Hold[set[LHS,RHS]]/.Inactive[funct_]:>funct/.set->Set//ReleaseHold]


(* Acts listwise on an expression and (optionally) it memoizes the result *)
Options[listwise] = {memoize -> True, progressBar -> True};
listwise[operator_, OptionsPattern[]][expr_,args___] := Module[{expanded, list, prog=0, len},
	expanded = Expand[expr];
	list = If[Head[expanded]===Plus, List@@expanded, {expanded}];
	len = Length[list];
	If[OptionValue[progressBar], Echo[ProgressIndicator[Dynamic[prog],{0,len}]]];
	
	If[OptionValue[memoize],
		Plus@@Map[
			(prog++;memoizeAndEvaluate[operator][#,args])&, list
		],
		Plus@@Map[
			(prog++;operator[#,args])&, list
		]
	]
];


(* ::Subsection::Closed:: *)
(*Threads on different processes*)


processes = 2;
tasksPerProcess = 2;


workFolder = FileNameJoin[{$HomeDirectory,"Documents","Superspace_computations"}];
If[Not[DirectoryQ[workFolder]],CreateDirectory[workFolder, CreateIntermediateDirectories->True]];


bashLauncher = "#!/bin/bash

# The exit status for a missing licence is 85
exit=85

while [ $exit -eq 85 ]
do

    \"$@\"
    exit=$?
    if [ $exit -eq 85 ]; then
        echo \"Waiting 3 seconds and then retrying...\"
        sleep 3
    fi
done
";


mathScript[inputFiles_, outputFiles_] /; Length[inputFiles] == Length[outputFiles] := Module[{notebookName, iniString},
	
	notebookName = Quiet[Check[NotebookFileName[], "Superspace4d.wl"]];

	iniString = "Get[\"" <> notebookName <> "\"];";
	
	Do[
		iniString = iniString <> "
{function, expression} = Import[\"" <> inputFiles[[job]] <> "\"];
Export[\"" <> outputFiles[[job]] <> "\", function[expression]];
ClearAll[function, expression];

"
	,{job, 1, Length[inputFiles]}];
	
	iniString <> "Quit[]"
];


call[operator_, list_, process_] := Module[{input, tempIn, output, scriptM, scriptB},
	input = Table[
		FileNameJoin[{workFolder, "in_"<>Hash[operator,"Expression","HexString"]<>"_"<>ToString[process]<>"-"<>ToString[jobCount]<>".mx"}]
	,{jobCount, 1, Length[list]}];
	output = Table[
		FileNameJoin[{workFolder, "out_"<>Hash[operator,"Expression","HexString"]<>"_"<>ToString[process]<>"-"<>ToString[jobCount]<>".mx"}]
	,{jobCount, 1, Length[list]}];
	scriptB = FileNameJoin[{workFolder, "bashWrapper.sh"}];
	scriptM = FileNameJoin[{workFolder, "in_"<>Hash[operator,"Expression","HexString"]<>"_"<>ToString[process]<>".m"}];
	
	Table[
		Export[input[[jobCount]], list[[jobCount]]]
	,{jobCount, 1, Length[list]}];
	If[Not[FileExistsQ[scriptB]],
		Export[scriptB,bashLauncher,"Text"];
		RunProcess[{"chmod","+x", scriptB}]];
	Export[scriptM,
		mathScript[input, output],
	"Text"];
	
	StartProcess[{"bash", scriptB, "math", "-noprompt", "-script", scriptM}];
	
	output	
];


thread[operator_][expr_] := Module[{expanded, list, len, assignment, proc, j$, repartition, procList, waitOutput, percentage, milestone, found},
	expanded = Expand[expr];
	list = If[Head[expanded]===Plus, List@@expanded, {expanded}];
	len = Length[list];
	
	assignment = Flatten[Table[Table[p,{t,tasksPerProcess}],{p,1,processes}]];
	repartition = Plus@@@Partition[list,UpTo[Ceiling[len/(tasksPerProcess processes)]]];
	
	procList = Table[{},{p,1,processes}];
	Do[
		assignment[[j$]] /. P_ :> AppendTo[procList[[P]], {operator, repartition[[j$]]}];
	,{j$, 1, Length[repartition]}];
	
	waitOutput = {};
	Do[
		waitOutput = Join[waitOutput, call[operator, procList[[p]], p]]
	,{p, 1, processes}];
	
	percentage = 0;
	milestone = -.01;
	
	While[percentage < 1,
		found = FileNames[waitOutput];
		percentage = Length[found] / Length[waitOutput];
		If[percentage > milestone,
			milestone = percentage + .1;
			WriteString["stdout", PercentForm[N[percentage],{2,0}]];
		];
		WriteString["stdout","."];		
		Pause[2]
	];
	WriteString["stdout","\ndone!"];
	
	Plus@@Table[
		Import[output]
	,{output, waitOutput}]

];


(* ::Section::Closed:: *)
(*Derivatives*)


(* ::Subsection::Closed:: *)
(*Preliminary definitions*)


dPower[f_,\[ScriptL]_]:=\[ScriptL] #^(\[ScriptL]-1) f[#,##2]&;


Leibniz[f_]:=Module[{$wrapper,factors},
	factors=$wrapper@@#;
    Plus@@Table[MapAt[Function[{x},f[x,##2]],factors,iter]/.$wrapper->Head[#],{iter,1,Length@factors}]
]&;


gradedLeibniz[f_]:=Module[{expr=#,temp,$wait},
	temp = Distribute[$wait@@expr];
	temp /. $wait[factors__] :> Sum[
		(-1)^(FN@@{factors}[[1;;iter-1]]) MapAt[Function[{x},f[x,##2]],$wait[factors],iter]
	,{iter, Length[{factors}]}] /. $wait -> NonCommutativeMultiply // productReduce
]&;


chainRule[diff_][f_,{argsF__},argsD__] := Module[{vects, len = Length[{argsF}]},
	vects = IdentityMatrix[len];
	(* We assume that the function f is bosonic *)
	FN[f[__]]:=0;
	FN[Derivative[__][f][__]]:=0;
	ordered\[Theta]List[f[argsF]]:=ordered\[Theta]List[NonCommutativeMultiply[argsF]];
	ordered\[Theta]List[Derivative[__][f][argsF]]:=ordered\[Theta]List[NonCommutativeMultiply[argsF]];
	Sum[diff[{argsF}[[arg]], argsD] Derivative[Sequence@@(vects[[arg]])][f][argsF], {arg, 1, len}]
]


(* ::Subsection::Closed:: *)
(*Elementary derivatives \[Eta] d\[Eta], \[Eta] d\[Theta], \[Theta] d\[Eta], \[Theta] d\[Theta] and x dx*)


(* Define the action of d\[Theta] or d\[Theta]b on a chain with \[Theta](b)'s *)
actWith\[Theta][c[ths__], \[Chi]_, theta_, odd_] := Module[{open\[Theta]},
	open\[Theta] = {\[Theta][ii_,jj_] :> \[Theta][ii] - \[Theta][jj], \[Theta]b[ii_,jj_] :> \[Theta]b[ii] - \[Theta]b[jj]};
	MapAt[D[# /. open\[Theta], theta] \[Chi] &, c[ths], 1]+
	MapAt[(-1)^(odd * FN[{ths}[[1]]]) D[# /. open\[Theta], theta] \[Chi] &, c[ths], -1] /. {
			c[___,0,___] -> 0,
			c[l___,- a_,r___] :> - c[l,a,r]
		}
];


(* Define the fermion numbers of this operator *)
\[Eta]d\[Eta]/:FN[\[Eta]d\[Eta][expr_,\[Chi]_,eta_]] := FN[expr];
(* Define the behavior on +, * and ** *)
\[Eta]d\[Eta][expr_Plus,\[Chi]_,eta_]:=linearity[\[Eta]d\[Eta]][expr,\[Chi],eta];
\[Eta]d\[Eta][expr_Times|expr_NonCommutativeMultiply,\[Chi]_,eta_]:=Leibniz[\[Eta]d\[Eta]][expr,\[Chi],eta];
\[Eta]d\[Eta][expr_^P_,\[Chi]_,eta_]:=dPower[\[Eta]d\[Eta],P][expr,\[Chi],eta];
\[Eta]d\[Eta][expr_,\[Chi]_,eta_] /; FreeQ[expr,eta] := 0;
(* Actual definition *)
\[Eta]d\[Eta][c[eta_,x___,\[Eta]p_],\[Chi]_,eta_] /; eta =!= \[Eta]p := c[\[Chi],x,\[Eta]p];
\[Eta]d\[Eta][c[\[Eta]p_,x___,eta_],\[Chi]_,eta_] /; eta =!= \[Eta]p := c[\[Eta]p,x,\[Chi]];
\[Eta]d\[Eta][c[eta_,x__,eta_],\[Chi]_,eta_] := c[\[Chi],x,eta] + c[eta,x,\[Chi]];
(* Define the behavior on a generic function *)
\[Eta]d\[Eta][f_[args__],\[Chi]_,eta_] /; MatchQ[f,altF] || MatchQ[f,Derivative[__][altF]] := chainRule[\[Eta]d\[Eta]][f,{args},\[Chi],eta];


(* Define the fermion numbers of this operator *)
\[Eta]d\[Theta]/:FN[\[Eta]d\[Theta][expr_,\[Chi]_,theta_]] := Mod[FN[expr]+1,2];
(* Define the behavior on +, * and ** *)
\[Eta]d\[Theta][expr_Plus,\[Chi]_,theta_]:=linearity[\[Eta]d\[Theta]][expr,\[Chi],theta];
\[Eta]d\[Theta][expr_Times|expr_NonCommutativeMultiply,\[Chi]_,theta_]:=gradedLeibniz[\[Eta]d\[Theta]][expr,\[Chi],theta];
\[Eta]d\[Theta][expr_^P_,\[Chi]_,theta_]:=dPower[\[Eta]d\[Theta],P][expr,\[Chi],theta];
\[Eta]d\[Theta][expr_,\[Chi]_,(head:\[Theta]|\[Theta]b)[i_]] /; FreeQ[expr,head[___,i,___]] := 0;
(* Actual definition (I assume it acts from the left) *)
\[Eta]d\[Theta][c[\[Eta]sAndx__],\[Chi]_,theta_] := actWith\[Theta][c[\[Eta]sAndx], \[Chi], theta, 1];
(* Define the behavior on a generic function *)
\[Eta]d\[Theta][f_[args__],\[Chi]_,theta_] /; MatchQ[f,altF] || MatchQ[f,Derivative[__][altF]] := chainRule[\[Eta]d\[Theta]][f,{args},\[Chi],theta];


(* Define the fermion numbers of this operator *)
\[Theta]d\[Eta]/:FN[\[Theta]d\[Eta][expr_,\[Chi]_,eta_]] := Mod[FN[expr]+1,2];
(* Define the behavior on +, * and ** *)
\[Theta]d\[Eta][expr_Plus,\[Chi]_,eta_]:=linearity[\[Theta]d\[Eta]][expr,\[Chi],eta];
\[Theta]d\[Eta][expr_Times|expr_NonCommutativeMultiply,\[Chi]_,eta_]:=gradedLeibniz[\[Theta]d\[Eta]][expr,\[Chi],eta];
\[Theta]d\[Eta][expr_^P_,\[Chi]_,eta_]:=dPower[\[Theta]d\[Eta],P][expr,\[Chi],eta];
\[Theta]d\[Eta][expr_,\[Chi]_,eta_] /; FreeQ[expr,eta] := 0;
(* Actual definition *)
\[Theta]d\[Eta][c[eta_,x___,\[Eta]p_],\[Chi]_,eta_] /; eta =!= \[Eta]p := c[\[Chi],x,\[Eta]p];
\[Theta]d\[Eta][c[\[Eta]p_,x___,eta_],\[Chi]_,eta_] /; eta =!= \[Eta]p := (-1)^FN[\[Eta]p] c[\[Eta]p,x,\[Chi]];
\[Theta]d\[Eta][c[eta_,x___,eta_],\[Chi]_,eta_] := c[\[Chi],x,eta] + c[eta,x,\[Chi]];
(* Define the behavior on a generic function *)
\[Theta]d\[Eta][f_[args__],\[Chi]_,eta_] /; MatchQ[f,altF] || MatchQ[f,Derivative[__][altF]] := chainRule[\[Theta]d\[Eta]][f,{args},\[Chi],eta];


(* Define the fermion numbers of this operator *)
\[Theta]d\[Theta]/:FN[\[Theta]d\[Theta][expr_,\[Chi]_,theta_]] := Mod[FN[expr],2];
(* Define the behavior on +, * and ** *)
\[Theta]d\[Theta][expr_Plus,\[Chi]_,theta_]:=linearity[\[Theta]d\[Theta]][expr,\[Chi],theta];
\[Theta]d\[Theta][expr_Times|expr_NonCommutativeMultiply,\[Chi]_,theta_]:=Leibniz[\[Theta]d\[Theta]][expr,\[Chi],theta];
\[Theta]d\[Theta][expr_^P_,\[Chi]_,theta_]:=dPower[\[Theta]d\[Theta],P][expr,\[Chi],theta];
\[Theta]d\[Theta][expr_,\[Chi]_,(head:\[Theta]|\[Theta]b)[i_]] /; FreeQ[expr,head[___,i,___]] := 0;
(* Actual definition *)
\[Theta]d\[Theta][c[\[Eta]sAndx__],\[Chi]_,theta_] := actWith\[Theta][c[\[Eta]sAndx], \[Chi], theta, 0];
(* Define the behavior on a generic function *)
\[Theta]d\[Theta][f_[args__],\[Chi]_,theta_] /; MatchQ[f,altF] || MatchQ[f,Derivative[__][altF]] := chainRule[\[Theta]d\[Theta]][f,{args},\[Chi],theta];


(* Define the action of dx on a product/trace/chain of x's *)
actWithX[$wrapper[xs__], s_, x[i_]] := Module[{openx},
	openx = {x[ii_,jj_] :> x[ii] - x[jj]};
	Table[
		MapAt[D[# /. openx, x[i]] s &, $wrapper[xs], index] /. {
			$wrapper[___,0,___] -> Nothing,
			$wrapper[l___,- a_,r___] :> - $wrapper[l,a,r]
		}
	,{index, 1, Length[{xs}]}]
];


(* Define the fermion numbers of this operator *)
xdx/:FN[xdx[expr_,s_,ex_]] := FN[expr];
(* Define the behavior on +, * and ** *)
xdx[expr_Plus,s_,ex_]:=linearity[xdx][expr,s,ex];
xdx[expr_Times|expr_NonCommutativeMultiply,s_,ex_]:=Leibniz[xdx][expr,s,ex];
xdx[expr_^P_,s_,ex_]:=dPower[xdx,P][expr,s,ex];
xdx[expr_,s_,x[i_]] /; FreeQ[expr,x[___,i,___]|u|v|z|zb] := 0;
(* Actual definition *)
xdx[d[xs__],s_,x[i_]] := Plus@@(actWithX[$wrapper[xs],s,x[i]] /. $wrapper -> d);
xdx[c[\[Eta]L_,xs__,\[Eta]R_],s_,x[i_]] := Plus@@(actWithX[$wrapper[xs],s,x[i]] /. $wrapper[xxs__] :> c[\[Eta]L,xxs,\[Eta]R]);
xdx[t[xs__],s_,x[i_]] := Plus@@(actWithX[$wrapper[xs],s,x[i]] /. $wrapper -> t);
xdx[e[xs__],s_,x[i_]] := Plus@@(actWithX[$wrapper[xs],s,x[i]] /. $wrapper -> e);
(* Define the behavior on a generic function *)
xdx[f_[args__],s_,ex_] /; MatchQ[f,altF] || MatchQ[f,Derivative[__][altF]] := chainRule[xdx][f,{args},s,ex];
(* Define behavior on cross ratios *)
xdx[u, s_, x[i_]] := Switch[i,
	1, 2u (d[s, x[1,2]]/d[x[1,2],x[1,2]]-d[s, x[1,3]]/d[x[1,3],x[1,3]]),
	2,-2u (d[s, x[1,2]]/d[x[1,2],x[1,2]]+d[s, x[2,4]]/d[x[2,4],x[2,4]]),
	3, 2u (d[s, x[3,4]]/d[x[3,4],x[3,4]]+d[s, x[1,3]]/d[x[1,3],x[1,3]]),
	4, 2u (-(d[s, x[3,4]]/d[x[3,4],x[3,4]])+d[s, x[2,4]]/d[x[2,4],x[2,4]]),
	_, 0];
xdx[v, s_, x[i_]] := Switch[i,
	1, 2v (d[s, x[1,4]]/d[x[1,4],x[1,4]]-d[s, x[1,3]]/d[x[1,3],x[1,3]]),
	2, 2v (d[s, x[2,3]]/d[x[2,3],x[2,3]]-d[s, x[2,4]]/d[x[2,4],x[2,4]]),
	3, 2v (-(d[s, x[2,3]]/d[x[2,3],x[2,3]])+d[s, x[1,3]]/d[x[1,3],x[1,3]]),
	4, 2v (-(d[s, x[1,4]]/d[x[1,4],x[1,4]])+d[s, x[2,4]]/d[x[2,4],x[2,4]]),
	_, 0];
xdx[z, s_, x[i_]] := (z-1)/(z-zb) xdx[u, s, x[i]] - z/(z-zb) xdx[v, s, x[i]];
xdx[zb, s_, x[i_]] := (1-zb)/(z-zb) xdx[u, s, x[i]] + zb/(z-zb) xdx[v, s, x[i]];


(* ::Subsection::Closed:: *)
(*Remove placeholders \[Chi] and s*)


remove\[Chi]::contractionFail="Some Grassmann variables have been lost in `1`.";


remove\[Chi][expr_,\[Chi]1_,\[Chi]2_] := Module[{expanded},
	expanded = fullExpand[expr];
	If[Head[expanded] === Plus,
		Plus@@(remove\[Chi]Aux[#,\[Chi]1,\[Chi]2]&/@(List@@expanded))
	,
		remove\[Chi]Aux[expanded,\[Chi]1,\[Chi]2]
	]
];


(* Computes \[PartialD]\[Chi]1\[PartialD]\[Chi]2 of a monomial, where \[Chi]1 and \[Chi]2 are placeholders indicating which spinors are being derived *)
remove\[Chi]Aux[expr_,\[Chi]1_,\[Chi]2_] := Module[{rules, uniqueized, oldordering, newordering, uptosigns, list, rev},
	rev[chainlist_] := Sequence@@Reverse[chainlist];
	{uniqueized, rules} = make\[Theta]sUnique[expr];
	oldordering = ordered\[Theta]List[uniqueized];
	uptosigns = uniqueized /. NonCommutativeMultiply -> Times;
	uptosigns = uptosigns // Rex[{
		c[\[Chi]1,x___,\[Chi]2] :> - (If[Head[\[Chi]1]===\[Chi], t[x], t@@(RotateLeft[{x}])] /. t[] -> 2),
		c[\[Chi]2,x___,\[Chi]1] :>   (If[Head[\[Chi]1]===\[Chi], t[x], t@@(RotateLeft[{x}])] /. t[] -> 2),
		c[\[Eta]L__,\[Chi]1]c[\[Chi]2,\[Eta]R__] :> c[\[Eta]L,\[Eta]R],
		c[\[Chi]1,\[Eta]L__]c[\[Chi]2,\[Eta]R__] :> (-1)^(Length[{\[Eta]L}]) c[rev[{\[Eta]L}],\[Eta]R],
		c[\[Chi]1,\[Eta]L__]c[\[Eta]R__,\[Chi]2] :> (-1)^(Length[{\[Eta]L}])(-1)^(Length[{\[Eta]R}]) c[rev[{\[Eta]L}], rev[{\[Eta]R}]],
		c[\[Eta]L__,\[Chi]1]c[\[Eta]R__,\[Chi]2] :> (-1)^(Length[{\[Eta]R}]) c[\[Eta]L, rev[{\[Eta]R}]]
	}];
	uptosigns = Expand[uptosigns /. Times -> NonCommutativeMultiply // productReduce];
	
	list = If[Head[uptosigns]===Plus,List@@(uptosigns),{uptosigns}];
	Plus@@(Block[{$temp},
		$temp=ordered\[Theta]List[#];
		tooMany\[Theta]Q[$temp /. rules] * Signature[PermutationList[FindPermutation[$temp,oldordering]]] * # /.rules
	] & /@ Select[list, Not[#===0]&])
];


removeS[expr_,s1_,s2_] := Module[{expanded},
	expanded = fullExpand[expr];
	If[Head[expanded] === Plus,
		Plus@@(removeSAux[#,s1,s2]&/@(List@@expanded))
	,
		removeSAux[expanded,s1,s2]
	]
];


(* Flips the order of the x's by taking care of eventual signs *)
flipChainOrTrace[c[\[Eta]L_,ex___,\[Eta]R_]] := (-1)^(Length[{ex}]+1) c[\[Eta]R,Sequence@@Reverse[{ex}],\[Eta]L];
flipChainOrTrace[t[ex__]] := t[Sequence@@Reverse[{ex}]];


(* Looks if a given s has indices up or down inside a given chain or trace (because in a product of x's the indices alternate up/down: x ~x x ~x ...) *)
(* The arguments are the entries of the chain or trace up to (and not including) the s in question *)
hasIndicesDown[c[(\[Eta]|\[Theta])[i_],l___]]   := EvenQ[Length[{l}]];
hasIndicesDown[c[(\[Eta]b|\[Theta]b)[i_],l___]] := OddQ[Length[{l}]];
hasIndicesDown[t[l___]]             := EvenQ[Length[{l}]];
(* With R-symmetry indices *)
(*hasIndicesDown[rc[\[Xi][a_],(\[Eta]|\[Theta])[i_],l___]]   := EvenQ[Length[{l}]];
hasIndicesDown[rc[\[Xi][a_],(\[Eta]b|\[Theta]b)[i_],l___]] := OddQ[Length[{l}]];
hasIndicesDown[rt[\[Theta][i_],l___]]             := EvenQ[Length[{l}]];
hasIndicesDown[rt[\[Theta]b[i_],l___]]            := OddQ[Length[{l}]];*)


(* Computes \[PartialD]s1\[PartialD]s2 of a monomial, where s1 and s2 are placeholders indicating which vectors are being derived *)
removeSAux[expr_,s1_,s2_] := Module[{rules, uniqueized, oldordering, newordering, uptosigns, list, rev},
	rev[chainlist_] := Sequence@@Reverse[chainlist];
	{uniqueized, rules} = make\[Theta]sUnique[expr];
	oldordering = ordered\[Theta]List[uniqueized];
	uptosigns = uniqueized /. NonCommutativeMultiply -> Times;
	uptosigns = uptosigns // fromEpsilonToTrace // Rex[{
		d[s1,s2] :> 4,
		d[s1,ex1_] d[s2,ex2_] :> d[ex1,ex2],
		
		d[s1,ex1_] c[\[Eta]L__,s2,\[Eta]R__] :> c[\[Eta]L,ex1,\[Eta]R],
		d[s2,ex1_] c[\[Eta]L__,s1,\[Eta]R__] :> c[\[Eta]L,ex1,\[Eta]R],
		
		d[s1,ex1_] t[xL___,s2,xR___] :> t[xL,ex1,xR],
		d[s2,ex1_] t[xL___,s1,xR___] :> t[xL,ex1,xR],
		
		
		c[\[Eta]L__,s1,xM___,s2,\[Eta]R__] /; EvenQ[Length[{xM}]]:> (-2)t[xM]c[\[Eta]L,\[Eta]R],
		c[\[Eta]L__,s2,xM___,s1,\[Eta]R__] /; EvenQ[Length[{xM}]]:> (-2)t[xM]c[\[Eta]L,\[Eta]R],
		c[\[Eta]L__,s1,xM___,s2,\[Eta]R__] /; OddQ[Length[{xM}]]:> 2 c[\[Eta]L,rev[{xM}],\[Eta]R],
		c[\[Eta]L__,s2,xM___,s1,\[Eta]R__] /; OddQ[Length[{xM}]]:> 2 c[\[Eta]L,rev[{xM}],\[Eta]R],
		
		t[xL___,s1,xM___,s2,xR___] /; EvenQ[Length[{xM}]]:> (-2)t[xM]t[xL,xR],
		t[xL___,s2,xM___,s1,xR___] /; EvenQ[Length[{xM}]]:> (-2)t[xM]t[xL,xR],
		t[xL___,s1,xM___,s2,xR___] /; OddQ[Length[{xM}]]:> 2 t[xL,rev[{xM}],xR],
		t[xL___,s2,xM___,s1,xR___] /; OddQ[Length[{xM}]]:> 2 t[xL,rev[{xM}],xR],
		
		
		c[\[Eta]L__,s1,\[Eta]R__] /; Not[hasIndicesDown[c[\[Eta]L]]] :> flipChainOrTrace[c[\[Eta]L,s1,\[Eta]R]],
		c[\[Eta]L__,s2,\[Eta]R__] /; hasIndicesDown[c[\[Eta]L]] :> flipChainOrTrace[c[\[Eta]L,s2,\[Eta]R]],
		
		t[xL___,s1,xR___] /; Not[hasIndicesDown[t[xL]]] :> flipChainOrTrace[t[xL,s1,xR]],
		t[xL___,s2,xR___] /; hasIndicesDown[t[xL]] :> flipChainOrTrace[t[xL,s2,xR]],
		
		c[\[Eta]L__,s1,\[Eta]R__]c[\[Eta]L2__,s2,\[Eta]R2__] /; hasIndicesDown[c[\[Eta]L]] && Not[hasIndicesDown[c[\[Eta]L2]]] :> c[\[Eta]L,\[Eta]R2]c[\[Eta]L2,\[Eta]R](-2),
		t[xL___,s1,xR___]t[xL2___,s2,xR2___] /; hasIndicesDown[c[xL]] && Not[hasIndicesDown[c[xL2]]] :> t[xL,xR2,xL2,xR](-2),
		
		t[xL___,s1,xR___]c[\[Eta]L2__,s2,\[Eta]R2__] /; hasIndicesDown[t[xL]] && Not[hasIndicesDown[c[\[Eta]L2]]] :> c[\[Eta]L2,xR,xL,\[Eta]R2](-2),
		c[\[Eta]L__,s1,\[Eta]R__]t[xL2___,s2,xR2___] /; hasIndicesDown[c[\[Eta]L]] && Not[hasIndicesDown[t[xL2]]] :> c[\[Eta]L,xR2,xL2,\[Eta]R](-2)
	}];
	uptosigns = Expand[uptosigns /. Times -> NonCommutativeMultiply // productReduce];
	
	list = If[Head[uptosigns]===Plus,List@@(uptosigns),{uptosigns}];
	Plus@@(Block[{$temp},
		$temp=ordered\[Theta]List[#];
		tooMany\[Theta]Q[$temp /. rules] * Signature[PermutationList[FindPermutation[$temp,oldordering]]] * # /. rules
	] & /@ Select[list, Not[#===0]&])

];


(* ::Subsection::Closed:: *)
(*d\[Eta] d\[Eta], d\[Theta] d\[Theta] and dx dx*)


(* \[PartialD]\[Eta]\[PartialD]\[Eta] *)
(* Define the properties *)
d\[Eta]d\[Eta]/:FN[d\[Eta]d\[Eta][expr_,\[Eta]1_,\[Eta]2_]]:=FN[expr];
d\[Eta]d\[Eta][expr_Plus,\[Eta]1_,\[Eta]2_]:=linearity[d\[Eta]d\[Eta]][expr,\[Eta]1,\[Eta]2];

(* Definition *)
d\[Eta]d\[Eta][expr_,\[Eta]1_,\[Eta]2_] /; FreeQ[expr,\[Eta]1] || FreeQ[expr,\[Eta]2] := 0;
d\[Eta]d\[Eta][expr_Times|expr_NonCommutativeMultiply|expr_c|expr_Power,\[Eta]1_,\[Eta]2_] := Module[{\[Chi]or\[Chi]b},
	\[Chi]or\[Chi]b = If[unbarredQ[\[Eta]1],\[Chi],\[Chi]b];
	remove\[Chi][\[Eta]d\[Eta][\[Eta]d\[Eta][expr,\[Chi]or\[Chi]b[2],\[Eta]2],\[Chi]or\[Chi]b[1],\[Eta]1],\[Chi]or\[Chi]b[1],\[Chi]or\[Chi]b[2]]
];
d\[Eta]d\[Eta][const_,\[Eta]1_,\[Eta]2_]/;constQ[const] := 0;


(* \[PartialD]\[Theta]\[PartialD]\[Theta] *)
(* Define the properties *)
d\[Theta]d\[Theta]/:FN[d\[Theta]d\[Theta][expr_,\[Theta]1_,\[Theta]2_]]:=FN[expr];
d\[Theta]d\[Theta][expr_Plus,\[Theta]1_,\[Theta]2_]:=linearity[d\[Theta]d\[Theta]][expr,\[Theta]1,\[Theta]2];

(* Definition *)
d\[Theta]d\[Theta][expr_Times|expr_NonCommutativeMultiply|expr_c|expr_Power,\[Theta]1_,\[Theta]2_] := Module[{\[Chi]or\[Chi]b},
	\[Chi]or\[Chi]b = If[unbarredQ[\[Theta]1],\[Chi],\[Chi]b];
	remove\[Chi][\[Eta]d\[Theta][\[Eta]d\[Theta][expr,\[Chi]or\[Chi]b[2],\[Theta]2],\[Chi]or\[Chi]b[1],\[Theta]1],\[Chi]or\[Chi]b[1],\[Chi]or\[Chi]b[2]]
];
d\[Theta]d\[Theta][const_,\[Theta]1_,\[Theta]2_]/;constQ[const] := 0;


(* \[PartialD]x\[PartialD]x *)
(* Define the properties *)
dxdx/:FN[dxdx[expr_,x1_,x2_]]:=FN[expr];
dxdx[expr_Plus,x1_,x2_]:=linearity[dxdx][expr,x1,x2];

(* Definition *)
dxdx[expr_Times|expr_NonCommutativeMultiply|expr_c|expr_d|expr_e|expr_t|expr_Power,x1_,x2_] := removeS[xdx[xdx[expr, s[2], x2], s[1], x1], s[1], s[2]];
dxdx[const_,x1_,x2_]/;constQ[const] := 0;


dxsq[expr_,x1_] := dxdx[expr,x1,x1];


(* ::Subsection::Closed:: *)
(*Composite derivatives*)


(* Yields a differential operator obtained by turning the objects listed in derivatives into actual derivatives *)
compositeD[op_, expr_] := Module[{derivatives, drule, \[Theta]list, \[Theta]blist, xlist, \[Eta]list, \[Eta]blist, $, remove$},

	drule = {d\[Theta] -> \[Theta]@*$, d\[Theta]b -> \[Theta]b@*$, dx -> x@*$, d\[Eta] -> \[Eta]@*$, d\[Eta]b -> \[Eta]b@*$};
	derivatives = Cases[op, d\[Theta][_]|d\[Theta]b[_]|dx[_]|d\[Eta][_]|d\[Eta]b[_], \[Infinity]] /. drule;

	\[Theta]list  = Cases[derivatives, \[Theta][_]];
	\[Theta]blist = Cases[derivatives, \[Theta]b[_]];
	xlist  = Cases[derivatives, x[_]];
	\[Eta]list  = Cases[derivatives, \[Eta][_]];
	\[Eta]blist = Cases[derivatives, \[Eta]b[_]];
	
	remove$[a__] := Sequence[a];
	
	(* for the \[Theta] derivatives we need to add signs because d\[Theta]d\[Theta] is supposed to act from the middle and not from the left *)
	Fold[
		d\[Eta]d\[Eta][#1, #2, #2 /. $ -> remove$] &,
		Fold[
			d\[Eta]d\[Eta][#1, #2, #2 /. $ -> remove$] &,
			Fold[
				dxdx[#1, #2, #2 /. $ -> remove$] &,
				Fold[
					-d\[Theta]d\[Theta][#1, #2, #2 /. $ -> remove$] &,
					Fold[
						-d\[Theta]d\[Theta][#1, #2, #2 /. $ -> remove$] &,
						(op /. drule) ** expr
					,\[Theta]list]
				,\[Theta]blist]
			,xlist]
		,\[Eta]list]
	,\[Eta]blist]
];


compositeD[op_] := (compositeD[op, #] &);


(* ::Section::Closed:: *)
(*Supercharges*)


(* If you want to symmetrize the Lorenz indices call a supercharge Q as Q[\[Theta], \[Eta]], whereas of you want to contract the indices call as Q[\[Theta], d\[Eta]] *)


(* Qs *)
opQ[point_, eta_][expr_] := compositeD[c[eta, d\[Theta][point]], expr] - I compositeD[c[eta,dx[point],\[Theta]b[point]], expr];
opQb[point_, etab_][expr_] := -compositeD[c[etab, d\[Theta]b[point]], expr] + I compositeD[c[\[Theta][point], dx[point], etab], expr];
(* Ds *)
opD[point_, eta_][expr_] := compositeD[c[eta, d\[Theta][point]], expr] + I compositeD[c[eta,dx[point],\[Theta]b[point]], expr];
opDb[point_, etab_][expr_] := -compositeD[c[etab, d\[Theta]b[point]], expr] - I compositeD[c[\[Theta][point], dx[point], etab], expr];


(* It will be useful to define all these terms separately *)
opQpTerm1[point_, eta_][expr_] := compositeD[-y[point] c[eta, d\[Theta][point]], expr];
opQpTerm2[point_, eta_][expr_] := 2 I compositeD[c[eta, dx[point], \[Theta]b[point]], expr];
(****)
opQbpTerm1[point_, etab_][expr_] := compositeD[-y[point] c[etab, d\[Theta]b[point]], expr];
opQbpTerm2[point_, etab_][expr_] := - 2 I compositeD[c[\[Theta][point], dx[point], etab], expr];


(* Qs in analytic superspace *)
opQp[point_, eta_][expr_] := opQpTerm1[point, eta][expr] + opQpTerm2[point, eta][expr];
opQm[point_, eta_][expr_] := compositeD[c[eta, d\[Theta][point]], expr];


(* Qbs in analytic superspace *)
opQbp[point_, etab_][expr_] := opQbpTerm1[point, etab][expr] + opQbpTerm2[point, etab][expr];
opQbm[point_, etab_][expr_] := compositeD[c[etab, d\[Theta]b[point]], expr];


(* Some signs I am worried about *)
coef[1] = 1;
coef[2] = 1;


(* Derivative with respect to y *)
Dy[expr_, y[point_]] := Module[{ys},
	ys = DeleteDuplicates[Cases[expr,y[___,point,___],{0,\[Infinity]}]];
	Sum[
	 D[expr, yvar](yvar /. {y[point]->1, y[_,point]->-1, y[point,_]->1})
	,{yvar, ys}]
]


(* It will be useful to define all these terms separately *)
opSpTerm1[point_, eta_][expr_] := 2 coef[1] sq[\[Theta][point]] compositeD[c[eta, d\[Theta][point]], expr] ;
opSpTerm2[point_, eta_][expr_] := 4y[point] c[\[Theta][point], eta] Dy[expr, y[point]];
opSpTerm3[point_, eta_][expr_] := I y[point] compositeD[c[d\[Theta]b[point], x[point], eta], expr];
opSpTerm4[point_, eta_][expr_] := 2 Module[{temp}, compositeD[c[\[Theta][point], dx[point], x[temp[point]], eta], expr]/.temp[x_]:>x];
(**)
opSmTerm1[point_, eta_][expr_] := -4 c[\[Theta][point], eta] Dy[expr, y[point]];
opSmTerm2[point_, eta_][expr_] := - I compositeD[c[d\[Theta]b[point], x[point], eta], expr];
(****)
opSbpTerm1[point_, etab_][expr_] := -2 coef[2] sq[\[Theta]b[point]] compositeD[c[etab, d\[Theta]b[point]], expr];
opSbpTerm2[point_, etab_][expr_] := 4y[point] c[etab, \[Theta]b[point]] Dy[expr, y[point]];
opSbpTerm3[point_, etab_][expr_] := I y[point] compositeD[c[etab, x[point], d\[Theta][point]], expr];
opSbpTerm4[point_, etab_][expr_] := 2 Module[{temp}, compositeD[c[etab, x[temp[point]], dx[point], \[Theta]b[point]], expr]/.temp[x_]:>x];
opSbpTerm5[point_, etab_][expr_] :=-8 c[etab,\[Theta]b[point]]expr ;
(**)
opSbmTerm1[point_, etab_][expr_] := -4 c[etab, \[Theta]b[point]] Dy[expr, y[point]];
opSbmTerm2[point_, etab_][expr_] := - I compositeD[c[etab, x[point], d\[Theta][point]], expr];


(* Ss in analytic superspace *)
opSp[point_, eta_][expr_] := opSpTerm1[point, eta][expr] + opSpTerm2[point, eta][expr] + opSpTerm3[point, eta][expr] + opSpTerm4[point, eta][expr];
opSm[point_, eta_][expr_] := opSmTerm1[point, eta][expr] + opSmTerm2[point, eta][expr];


(* Sbs in analytic superspace *)
opSbp[point_, etab_][expr_] := opSbpTerm1[point, etab][expr] + opSbpTerm2[point, etab][expr] + opSbpTerm3[point, etab][expr] + opSbpTerm4[point, etab][expr];
opSbm[point_, etab_][expr_] := opSbmTerm1[point, etab][expr] + opSbmTerm2[point, etab][expr];


(* ::Section::Closed:: *)
(*Super-propagators*)


g[i_, j_]:= (y[i,j]-4 I c[\[Theta][i,j], x[i,j]/sq[x[i,j]],\[Theta]b[i,j]])/sq[x[i,j]]//allReduce;


(* ::Section::Closed:: *)
(*Comparing expressions*)


(* ::Subsection::Closed:: *)
(*Definition of the numerical values*)


sigma[0]={{-1,0},{0,-1}}; sigma[1]={{0,1},{1,0}}; sigma[2]={{0,-I},{I,0}}; sigma[3]={{1,0},{0,-1}};
sigmab[0]=sigma[0];sigmab[1]=-sigma[1];sigmab[2]=-sigma[2];sigmab[3]=-sigma[3];
metric = {-1,1,1,1};


(* \[Epsilon]_12 = -1, \[Epsilon]^12 = 1 *)
lowerSpinor[{a_,b_}] := {-b,a};


(* \[Eta]xxx...(\[Eta] or \[Eta]b) *)
ncEta[\[Eta]L_,xs___,\[Eta]R_] := \[Eta]L . Dot@@(alternateMap[
	{
	#[[1]] sigma[0] + #[[2]] sigma[1] + #[[3]] sigma[2] + #[[4]] sigma[3]&,
	#[[1]] sigmab[0] + #[[2]] sigmab[1] + #[[3]] sigmab[2] + #[[4]] sigmab[3]&
	}
,{xs}]) . If[EvenQ[Length[{xs}]],lowerSpinor[\[Eta]R],\[Eta]R];
(* \[Eta]bxxx...(\[Eta] or \[Eta]b) *)
ncEtab[\[Eta]L_,xs___,\[Eta]R_] := lowerSpinor[\[Eta]L] . Dot@@(alternateMap[
	{
	#[[1]] sigmab[0] + #[[2]] sigmab[1] + #[[3]] sigmab[2] + #[[4]] sigmab[3]&,
	#[[1]] sigma[0] + #[[2]] sigma[1] + #[[3]] sigma[2] + #[[4]] sigma[3]&
	}
,{xs}]) . If[OddQ[Length[{xs}]],lowerSpinor[\[Eta]R],\[Eta]R];
(* x1.x2 *)
nd[x1_,x2_] := Sum[x1[[mu]] metric[[mu]] x2[[mu]],{mu,4}];
(* trace *)
nt[xs__] := Tr[Dot@@(alternateMap[
	{
	#[[1]] sigma[0] + #[[2]] sigma[1] + #[[3]] sigma[2] + #[[4]] sigma[3]&,
	#[[1]] sigmab[0] + #[[2]] sigmab[1] + #[[3]] sigmab[2] + #[[4]] sigmab[3]&
	}
,{xs}])];
nt[] := 2;
(* \[Epsilon] *)
ne[x1_,x2_,x3_,x4_] := Sum[LeviCivitaTensor[4][[k,l,m,n]]x1[[k]]x2[[l]]x3[[m]]x4[[n]],{k,1,4},{l,1,4},{m,1,4},{n,1,4}];


(* If True this forces to find values that are in the branch 0 < z < 1, 0 < zb < 1 with 0 < u < 1 - 2Sqrt[v] + v and 0 < v < 1 *)
Options[makeRule] = {correctBranch -> False};
makeRule::npoints = "For the option correctBranch = True one can only use 4 points; I have found `1`";


(* Replacement rule for random rational values *)
denom = 19;

makeRule[xlist_, \[Eta]list_, \[Theta]list_, OptionsPattern[]] := Module[{cb = OptionValue[correctBranch], uvInBranch, \[Eta]\[Eta] = DiagonalMatrix[{-1,1,1,1}], rand1, rand2, rand3, rand4},

	uvInBranch[x1_, x2_, x3_, x4_] := With[{uuu = ((x1-x2) . \[Eta]\[Eta] . (x1-x2)(x3-x4) . \[Eta]\[Eta] . (x3-x4))/((x1-x3) . \[Eta]\[Eta] . (x1-x3)(x2-x4) . \[Eta]\[Eta] . (x2-x4)), vvv = ((x1-x4) . \[Eta]\[Eta] . (x1-x4)(x2-x3) . \[Eta]\[Eta] . (x2-x3))/((x1-x3) . \[Eta]\[Eta] . (x1-x3)(x2-x4) . \[Eta]\[Eta] . (x2-x4))}, 
		0 < vvv < 1 && 0 < uuu < 1-2 Sqrt[vvv]+vvv
	];

	If[Not[cb],
		Join[xlist, \[Eta]list, \[Theta]list] /. {
			x[i_] :> Sequence@@(Thread[{x0[i], x1[i], x2[i], x3[i]} -> (RandomInteger[{-denom,denom},4]/denom)]),
			\[Eta][i_] :> Sequence@@(Thread[{\[Eta]1[i], \[Eta]2[i], \[Eta]3[i], \[Eta]4[i]} -> (RandomInteger[{-denom,denom},4]/denom)]),
			\[Theta][i_] :> Sequence@@(Thread[{\[Theta]1[i], \[Theta]2[i], \[Theta]3[i], \[Theta]4[i]} -> (RandomInteger[{-denom,denom},4]/denom)])
		},
		If[Length[xlist]>4, Message[makeRule::npoints, Length[xlist]]];
		rand1=RandomInteger[{-denom,denom},4]/denom;
		rand2=RandomInteger[{-denom,denom},4]/denom;
		rand3=RandomInteger[{-denom,denom},4]/denom;
		rand4=RandomInteger[{-denom,denom},4]/denom;
		While[Not[uvInBranch[rand1, rand2, rand3, rand4]],
			rand1=RandomInteger[{-denom,denom},4]/denom;
			rand2=RandomInteger[{-denom,denom},4]/denom;
			rand3=RandomInteger[{-denom,denom},4]/denom;
			rand4=RandomInteger[{-denom,denom},4]/denom;
		];
		Join[xlist, \[Eta]list, \[Theta]list] /. {
			x[1] :> Sequence@@(Thread[{x0[1], x1[1], x2[1], x3[1]} -> (rand1)]),
			x[2] :> Sequence@@(Thread[{x0[2], x1[2], x2[2], x3[2]} -> (rand2)]),
			x[3] :> Sequence@@(Thread[{x0[3], x1[3], x2[3], x3[3]} -> (rand3)]),
			x[4] :> Sequence@@(Thread[{x0[4], x1[4], x2[4], x3[4]} -> (rand4)]),
			\[Eta][i_] :> Sequence@@(Thread[{\[Eta]1[i], \[Eta]2[i], \[Eta]3[i], \[Eta]4[i]} -> (RandomInteger[{-denom,denom},4]/denom)]),
			\[Theta][i_] :> Sequence@@(Thread[{\[Theta]1[i], \[Theta]2[i], \[Theta]3[i], \[Theta]4[i]} -> (RandomInteger[{-denom,denom},4]/denom)])
		}
	]
];

randomReplacement[expr_] := Module[{runningx = {}, runningy = {}, running\[Eta] = {}, running\[Theta] = {}, ready},
	ready = expr /. {
		x[i_,j_] :> x[i] - x[j],
		\[Theta][i_,j_] :> \[Theta][i] - \[Theta][j],
		\[Theta]b[i_,j_] :> \[Theta]b[i] - \[Theta]b[j],
		y[i_,j_] :> y[i] - y[j]} /. {
		x[i_]   :> (runningx=Union[runningx,{x[i]}];   {x0[i], x1[i], x2[i], x3[i]}),
		\[Eta][i_]   :> (running\[Eta]=Union[running\[Eta],{\[Eta][i]}];   {\[Eta]1[i] + I \[Eta]2[i], \[Eta]3[i] + I \[Eta]4[i]}),
		\[Eta]b[i_]  :> (running\[Eta]=Union[running\[Eta],{\[Eta][i]}];   {\[Eta]1[i] - I \[Eta]2[i], \[Eta]3[i] - I \[Eta]4[i]}),
		\[Theta][i_]  :> (running\[Theta]=Union[running\[Theta],{\[Theta][i]}];   {\[Theta]1[i] + I \[Theta]2[i], \[Theta]3[i] + I \[Theta]4[i]}),
		\[Theta]b[i_] :> (running\[Theta]=Union[running\[Theta],{\[Theta][i]}];   {\[Theta]1[i] - I \[Theta]2[i], \[Theta]3[i] - I \[Theta]4[i]})
	};
	{ready, runningx, running\[Eta], running\[Theta]}
];


(* ::Subsection::Closed:: *)
(*Compare an expression to zero*)


Options[compare]={expReplacement -> {(* q->2,qb\[Rule]2,\[ScriptL]->2,l->2,k->1,\[CapitalDelta]\[Rule]4,j->0,\[CapitalDelta]\[Phi]->1 *)},(* Replacements for the exponents of e.g. x12sq^\[CapitalDelta] *)
                  verbose -> 0, (* Displays more info on the system that is being solved *)
                  minEqns -> -1, (* Writes at least minEqns equations, if -1 the number of equations equals the number of variables matched *)
                  onlyShow -> False}; (* If true shows the numerical substitution but doesn't solve anyhting *)
                  
(* Puts an overall minus to terms that contain \[Theta]s or \[Theta]bs in a non-canonical order, so that the replacement of numerical values is consistent *)
fixSigns::zerosign = "The expression `1` has repeated \[Theta]'s that should have been reduced earlier";
fixSigns[a_ * ex_] /; FreeQ[a, \[Theta]|\[Theta]b]:=a fixSigns[ex];
fixSigns[a_] /; FreeQ[a, \[Theta]|\[Theta]b]:=a;
fixSigns[expr_Plus]:=linearity[fixSigns][expr];
fixSigns[mon_Times|mon_NonCommutativeMultiply|mon_c]:= Module[{sig = Signature[ordered\[Theta]List[mon]]},
	If[sig==0, Message[fixSigns::zerosign, mon]];
	sig * (mon/. NonCommutativeMultiply -> Times)
];


(* Compares an expression to zero *)
compare[expr_, vars_, options: OptionsPattern[]] := Module[{vPrint, zeroExpr, \[Theta]squares={}, xlist, \[Eta]list, \[Theta]list, eqnlist={}, hasZ},
	vPrint[str_,verb_:0] := If[(OptionValue[verbose]/.{True->1,False->0})>verb,Print[str],Null];
	
	zeroExpr = fixSigns[equal\[Theta]Reduce[expr] /. {
		c[\[Theta][i__],\[Theta][i__]] :>   (\[Theta]squares = Union[\[Theta]squares, {\[Theta]sq[i]}]; \[Theta]sq[i]),
		c[\[Theta]b[i__],\[Theta]b[i__]] :> (\[Theta]squares = Union[\[Theta]squares, {\[Theta]bsq[i]}]; \[Theta]bsq[i])}];
		
	zeroExpr = Flatten[{CoefficientList[zeroExpr, \[Theta]squares]}];
	If[FreeQ[zeroExpr,z|zb], hasZ = False, hasZ = True];
	zeroExpr = zeroExpr /. {Power[b_,e_]:>Power[b,e/.OptionValue[expReplacement]]} /. {
		c[\[Eta][i_],j__] :>   cEta[\[Eta][i],j],
		c[\[Eta]b[i_],j__] :>  cEtab[\[Eta]b[i],j],
		c[\[Theta][i__],j__] :>  cEta[\[Theta][i],j],
		c[\[Theta]b[i__],j__] :> cEtab[\[Theta]b[i],j]
	} /. If[hasZ, {
		(1-z)(1-zb) -> v,
		(z-1)(zb-1) -> v,
		z zb -> u,
		z+zb -> 1+u-v
	},{}]/. If[hasZ, {
		z -> 1/2 (1+u-Sqrt[-4 u+(1+u-v)^2]-v),
		zb -> 1/2 (1+u+Sqrt[-4 u+(1+u-v)^2]-v)
	},{}]/. {
		u -> (sq[x[1,2]]sq[x[3,4]])/(sq[x[1,3]]sq[x[2,4]]),
		v -> (sq[x[1,4]]sq[x[2,3]])/(sq[x[1,3]]sq[x[2,4]])
	};
	If[hasZ && Not[OptionValue[onlyShow]], zeroExpr=Simplify[zeroExpr]];
	
	{zeroExpr, xlist, \[Eta]list, \[Theta]list} = randomReplacement[zeroExpr];
	vPrint[StringJoin["Replacing numerical values to ",ToString[\[Eta]list,InputForm]," ",ToString[\[Theta]list,InputForm]," ",ToString[xlist,InputForm],"."]];
	If[Length[\[Theta]squares] > 0,
		vPrint[StringJoin["Combining equations from \[Theta] squares: ",ToString[\[Theta]squares,InputForm],"."]];
	];
	
	Do[
		AppendTo[eqnlist, zeroExpr /. makeRule[xlist, \[Eta]list, \[Theta]list, correctBranch -> hasZ] /. {
			cEta -> ncEta, cEtab -> ncEtab, t -> nt, e -> ne, d -> nd
		}];
		vPrint[Variables[Last[eqnlist]],1];
		vPrint[Last[eqnlist],2];
	, Max[Length@vars,OptionValue[minEqns]]];
	
	If[OptionValue[onlyShow], Return[N /@ Flatten[eqnlist]]];
	
	vPrint["Solving for "<>ToString[vars,InputForm]<>" ..."];
	Solve[
        #==0 & /@ DeleteCases[Collect[#, vars] & /@ Flatten[eqnlist], 0]
        ,vars
    ] /. Rule[symb_,something_] :> Rule[symb,Collect[something, vars, Factor]]	
];


(* This is for when sometimes we get zeros by chance *)
SetAttributes[tryOverAndOver,HoldAll];
MAXTRIALS=10;
tryOverAndOver[expr_]:=Module[{S=Exception[],counter=0},
	While[S===Exception[] && counter < MAXTRIALS,
		If[counter>0,Print["attempt ",counter+1]];
		S=Quiet[Check[expr,Exception[],{Thread::tdlen,Power::infy}]];
		counter++;
	];
	S
];


(* ::Section::Closed:: *)
(*Non supersymmetric correlators*)


(* ::Subsection::Closed:: *)
(*Two-point function*)


nonSUSY2pf[\[CapitalDelta]_,j_,jb_] := (I)^(j+jb) (c[\[Eta][1],x[1,2],\[Eta]b[2]]^j c[\[Eta][2],x[1,2],\[Eta]b[1]]^jb)/sq[x[1,2]]^(\[CapitalDelta]+(j+jb)/2);


(* ::Subsection::Closed:: *)
(*Three-point function*)


(* This uses CFTs4D` *)


(*Xij*)
myspM[i_,j_] := sq[x[i,j]];

(*I^ij*)
myinvSbSnorm[{i_,j_}, {}] := c[\[Eta][j],x[i,j],\[Eta]b[i]];

(*J^k_ij*)
myinvSbSnorm[{k_,k_}, {i_, j_}] := (sq[x[i,k]] sq[x[j,k]])/sq[x[i,j]] (c[\[Eta][k],x[i,k],\[Eta]b[k]]/sq[x[i,k]]-c[\[Eta][k],x[j,k],\[Eta]b[k]]/sq[x[j,k]]);

(*K^ij_k*)
myinvSSnorm[{i_,j_},{k_}] := - Sqrt[(sq[x[i,j]]/(sq[x[i,k]]sq[x[j,k]]))]c[\[Eta][i],x[i,k],x[j,k],\[Eta][j]];

(*Kb^ij_k*)
myinvSbSbnorm[{i_,j_},{k_}] := - Sqrt[(sq[x[i,j]]/(sq[x[i,k]]sq[x[j,k]]))]c[\[Eta]b[i],x[i,k],x[j,k],\[Eta]b[j]];

(*I^ij_kl*)
myinvSbSnorm[{i_,j_}, {k_,l_}] := 1/sq[x[k,l]] c[\[Eta]b[i],x[i,k],x[k,l],x[j,l],\[Eta][j]];

(*L^i_jkl*)
myinvSSnorm[{i_,i_},{j_,k_,l_}] := 1/Sqrt[sq[x[j,k]]sq[x[k,l]]sq[x[j,l]]] c[\[Eta][i],x[i,j],x[j,k],x[k,l],x[i,l],\[Eta][i]];

(*Lb^i_jkl*)
myinvSbSbnorm[{i_,i_},{j_,k_,l_}] := 1/Sqrt[sq[x[j,k]]sq[x[k,l]]sq[x[j,l]]] c[\[Eta]b[i],x[i,j],x[j,k],x[k,l],x[i,l],\[Eta]b[i]];


FromCFTs4D[expr_]:=expr/.{CFTs4D`spM | spM -> myspM,
                          CFTs4D`invSbSnorm | invSbSnorm -> myinvSbSnorm,
                          CFTs4D`invSSnorm | invSSnorm  -> myinvSSnorm,
                          CFTs4D`invSbSbnorm | invSbSbnorm -> myinvSbSbnorm};


nonSUSY3pf[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}] := FromCFTs4D[CFTs4D`n3CorrelationFunction[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}]];


(* ::Subsection::Closed:: *)
(*From the reduced three-point function to position space*)


(* This applies the prefactor \[ScriptCapitalK]_O1O2 to the three-point function in the representation t(X). *)
(* X is either X_3, X_2 or X_1, which are denoted, respectively, as x[3], x[3] and x[1]. *)
applyPrefactor::exes="The expression for t must contain only one among x[1], x[2] or x[3]. Now it contains `1`.";
applyPrefactor[{\[CapitalDelta]1_,j1_,jb1_}, {\[CapitalDelta]2_,j2_,jb2_}, tpf_] := Module[{
	chosenX, $sequence, Xlow, Xup, Xsq, numerator, denominator, Xdenom, rule\[Chi], points12, final},
	chosenX = Cases[tpf,x[1|2|3],\[Infinity]] // DeleteDuplicates;
	If[Length[chosenX] != 1,
		Message[applyPrefactor::exes, ToString[chosenX,InputForm]]; Return[Null];,
		chosenX = chosenX[[1]];
	];
	Switch[chosenX
	,x[1], 
		Xlow = $sequence[x[1,2],x[2,3],x[3,1]];
		Xup  = $sequence[x[3,1],x[2,3],x[1,2]];
		Xdenom = sq[x[1,3]]sq[x[1,2]];
		Xsq = sq[x[2,3]] / (sq[x[1,3]]sq[x[1,2]]);
		numerator = {x[1,2],x[1,3]};
		denominator = sq[x[1,2]]^(\[CapitalDelta]1 + 1/2 (j1+jb1)) sq[x[1,3]]^(\[CapitalDelta]2 + 1/2 (j2+jb2));
		rule\[Chi] = {\[Eta][2] -> \[Chi][1], \[Eta][3] -> \[Chi][2], \[Eta]b[2] -> \[Chi]b[1], \[Eta]b[3] -> \[Chi]b[2]};
		points12 = {2,3};
	,x[2],
		Xlow = $sequence[x[2,3],x[3,1],x[1,2]];
		Xup  = $sequence[x[1,2],x[3,1],x[2,3]];
		Xsq = sq[x[1,3]] / (sq[x[2,3]]sq[x[1,2]]);
		Xdenom = sq[x[2,3]]sq[x[1,2]];
		numerator = {x[1,2],x[3,2]};
		denominator = sq[x[1,2]]^(\[CapitalDelta]1 + 1/2 (j1+jb1)) sq[x[2,3]]^(\[CapitalDelta]2 + 1/2 (j2+jb2));
		rule\[Chi] = {\[Eta][1] -> \[Chi][1], \[Eta][3] -> \[Chi][2], \[Eta]b[1] -> \[Chi]b[1], \[Eta]b[3] -> \[Chi]b[2]};
		points12 = {1,3};
	,x[3],
		Xlow = $sequence[x[3,1],x[1,2],x[2,3]];
		Xup  = $sequence[x[2,3],x[1,2],x[3,1]];
		Xsq = sq[x[1,2]] / (sq[x[1,3]]sq[x[2,3]]);
		Xdenom = sq[x[1,3]]sq[x[2,3]];
		numerator = {x[1,3],x[2,3]};
		denominator = sq[x[1,3]]^(\[CapitalDelta]1 + 1/2 (j1+jb1)) sq[x[2,3]]^(\[CapitalDelta]2 + 1/2 (j2+jb2));
		rule\[Chi] = {\[Eta][1] -> \[Chi][1], \[Eta][2] -> \[Chi][2], \[Eta]b[1] -> \[Chi]b[1], \[Eta]b[2] -> \[Chi]b[2]};
		points12 = {1,2};
	];
	final = (tpf /. d[chosenX, chosenX] -> Xsq /. {
		c[\[Eta][i_], xx__, \[Eta]R_] :> c[\[Eta][i], Sequence@@alternateMap[
			{(# /. chosenX -> Xlow)&,
			 (# /. chosenX -> Xup)&},
			 {xx}
		], \[Eta]R] (Xdenom)^(-Length[{xx}]),
		c[\[Eta]b[i_],xx__, \[Eta]R_] :> c[\[Eta]b[i], Sequence@@alternateMap[
			{(# /. chosenX -> Xup)&,
			 (# /. chosenX -> Xlow)&},
			 {xx}
		], \[Eta]R](Xdenom)^(-Length[{xx}]),
		t[xx__] :> t[Sequence@@alternateMap[
			{(# /. chosenX -> Xlow)&,
			 (# /. chosenX -> Xup)&},
			 {xx}
		]] (Xdenom)^(-Length[{xx}])
	} /. $sequence -> Sequence /. rule\[Chi] //. {
		c[\[Chi][i_], r__]  :> c[\[Eta]b[points12[[i]]], numerator[[i]], r],
		c[\[Chi]b[i_], r__] :> c[\[Eta][points12[[i]]],  numerator[[i]], r],
		c[l__, \[Chi][i_]]  :> c[l, numerator[[i]], \[Eta]b[points12[[i]]]],
		c[l__, \[Chi]b[i_]] :> c[l, numerator[[i]], \[Eta][points12[[i]]]]
	}) / denominator
]


(* ::Subsection::Closed:: *)
(*To conformal frame*)


(* ::Subsubsection::Closed:: *)
(*Definitions in CFTs4D*)


(* These functions are adapted from CFTs4D https://gitlab.com/bootstrapcollaboration/CFTs4D for compatibility issues *)
myCollapseCFStructs[_][0]:=0;
myCollapseCFStructs[\[CapitalDelta]s_][expr_]:=Module[
{
	tmp=Expand[expr,CF\[Xi][_]|CF\[Xi]b[_]|CF\[Eta][_]|CF\[Eta]b[_]]
	,\[Xi]s,\[Xi]bs
	,\[Eta]s,\[Eta]bs
	,qs,qbs
	,ls,lbs
	,i
},
	If[tmp===0,Return[0]];
	tmp=If[Head[tmp]=!=Plus,{tmp},List@@tmp];
	\[Xi]s=Expand@Table[Exponent[#,CF\[Xi][i]],{i,1,4}]&/@tmp;
	\[Xi]bs=Expand@Table[Exponent[#,CF\[Xi]b[i]],{i,1,4}]&/@tmp;
	\[Eta]s=Expand@Table[Exponent[#,CF\[Eta][i]],{i,1,4}]&/@tmp;
	\[Eta]bs=Expand@Table[Exponent[#,CF\[Eta]b[i]],{i,1,4}]&/@tmp;
	tmp=tmp/.{CF\[Xi][_]->1,CF\[Xi]b[_]->1,CF\[Eta][_]->1,CF\[Eta]b[_]->1};
	
	qs=Expand[(\[Eta]s-\[Xi]s)/2];
	qbs=Expand[(\[Eta]bs-\[Xi]bs)/2];
	ls=Expand[\[Eta]s+\[Xi]s];
	lbs=Expand[\[Eta]bs+\[Xi]bs];
	
	tmp=Plus@@(Table[CFTs4D`CF4pt[\[CapitalDelta]s,ls[[i]],lbs[[i]],qs[[i]],qbs[[i]],1]tmp[[i]],{i,1,Length@qs}]);
	Collect[tmp,CFTs4D`CF4pt[___],Simplify]
]


(*We form pairs of the type A*)
listPairsABasic={CF\[Xi][i_] CF\[Xi]b[j_]:>P\[Xi]\[Xi]b[i,j],CF\[Eta][i_] CF\[Eta]b[j_]:>P\[Eta]\[Eta]b[i,j]};

listPairsAExtended={
	CF\[Xi][i_]^m_ CF\[Xi]b[j_]:>(CF\[Xi][i]CF\[Xi]b[j]/.listPairsABasic)CF\[Xi][i]^(m-1),
	CF\[Xi][i_]CF\[Xi]b[j_]^m_:>(CF\[Xi][i]CF\[Xi]b[j]/.listPairsABasic)CF\[Xi]b[j]^(m-1),
	CF\[Xi][i_]^m_ CF\[Xi]b[j_]^n_:>(CF\[Xi][i]CF\[Xi]b[j]/.listPairsABasic)^Min[m,n]CF\[Xi][i]^(m-Min[m,n])CF\[Xi]b[j]^(n-Min[m,n]),
	
	CF\[Eta][i_]^m_ CF\[Eta]b[j_]:>(CF\[Eta][i]CF\[Eta]b[j]/.listPairsABasic)CF\[Eta][i]^(m-1),
	CF\[Eta][i_]CF\[Eta]b[j_]^m_:>(CF\[Eta][i]CF\[Eta]b[j]/.listPairsABasic)CF\[Eta]b[j]^(m-1),
	CF\[Eta][i_]^m_ CF\[Eta]b[j_]^n_:>(CF\[Eta][i]CF\[Eta]b[j]/.listPairsABasic)^Min[m,n]CF\[Eta][i]^(m-Min[m,n])CF\[Eta]b[j]^(n-Min[m,n])
};

listPairsA=Union[listPairsABasic,listPairsAExtended];

(*We form pairs of the type B*)
listPairsBBasic={CF\[Xi][i_] CF\[Eta][j_]:>P\[Xi]\[Eta][i,j],CF\[Xi]b[i_] CF\[Eta]b[j_]:>P\[Xi]b\[Eta]b[i,j]};

listPairsBExtended={
	CF\[Xi][i_]^m_ CF\[Eta][j_]:>(CF\[Xi][i]CF\[Eta][j]/.listPairsBBasic)CF\[Xi][i]^(m-1),
	CF\[Xi][i_]CF\[Eta][j_]^m_:>(CF\[Xi][i]CF\[Eta][j]/.listPairsBBasic)CF\[Eta][j]^(m-1),
	CF\[Xi][i_]^m_ CF\[Eta][j_]^n_:>(CF\[Xi][i]CF\[Eta][j]/.listPairsBBasic)^Min[m,n]CF\[Xi][i]^(m-Min[m,n])CF\[Eta][j]^(n-Min[m,n]),
	
	CF\[Xi]b[i_]^m_ CF\[Eta]b[j_]:>(CF\[Xi]b[i]CF\[Eta]b[j]/.listPairsBBasic)CF\[Xi]b[i]^(m-1),
	CF\[Xi]b[i_]CF\[Eta]b[j_]^m_:>(CF\[Xi]b[i]CF\[Eta]b[j]/.listPairsBBasic)CF\[Eta]b[j]^(m-1),
	CF\[Xi]b[i_]^m_ CF\[Eta]b[j_]^n_:>(CF\[Xi]b[i]CF\[Eta]b[j]/.listPairsBBasic)^Min[m,n]CF\[Xi]b[i]^(m-Min[m,n])CF\[Eta]b[j]^(n-Min[m,n])
};

listPairsB=Union[listPairsBBasic,listPairsBExtended];

(*Replace products with pairs*)
myToFormPairs[expr_]:=(expr//.listPairsA)//.listPairsB;

ConnectionCFEFnorm:=Import[FileNameJoin[{NotebookDirectory[],"ConnectionCFEFnorm.m"}]];


myExpandCFStructs[expr_]:=expr/.{
	CFTs4D`CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>g Module[{i},
		Product[CF\[Xi][i]^(-qs[[i]]+ls[[i]]/2) CF\[Eta][i]^(qs[[i]]+ls[[i]]/2) CF\[Xi]b[i]^(-qbs[[i]]+lbs[[i]]/2) CF\[Eta]b[i]^(qbs[[i]]+lbs[[i]]/2),{i,1,4}]
	]
};


(* ::Subsubsection::Closed:: *)
(*My functions*)


toCF::dimError = "The dimension `1` at point \!\(\*SubscriptBox[\(x\), \(4\)]\) is not correct: it goes like \[ScriptCapitalL]^`2` (I corrected it)";


toCF[\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_][expr_] := Module[{temp, \[ScriptCapitalL]},
	
	temp = expr /. {
		c[\[Eta][i_],j__] :>   cEta[\[Eta][i],j],
		c[\[Eta]b[i_],j__] :>  cEtab[\[Eta]b[i],j]
	} /. {
		x[i_,j_] :> x[i] - x[j]
	};
	
	temp = Normal[Series[
		\[ScriptCapitalL]^(2\[CapitalDelta]4) temp /. {
			x[1] -> {0,0,0,0},
			x[2] -> {(z-zb)/2,0,0,(z+zb)/2},
			x[3] -> {0,0,0,1},
			x[4] -> {0,0,0,\[ScriptCapitalL]},
			\[Eta][i_] :> {-CF\[Eta][i], CF\[Xi][i]},
			\[Eta]b[i_] :> {-CF\[Eta]b[i], CF\[Xi]b[i]} 
		} /. {
			cEta -> ncEta, cEtab -> ncEtab, t -> nt, e -> ne, d -> nd
		}
	,{\[ScriptCapitalL],\[Infinity],0}]];
	
	If[Not[FreeQ[temp,\[ScriptCapitalL]]],
		Message[toCF::dimError, \[CapitalDelta]4, Exponent[temp,\[ScriptCapitalL]]];
		temp = Normal[Series[temp \[ScriptCapitalL]^(-Exponent[temp,\[ScriptCapitalL]]),{\[ScriptCapitalL],\[Infinity],0}]]
	];
	
	myCollapseCFStructs[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]3}][temp]
];


(* This function assumes the branch 0 < z < 1 and 0 < zb < 1. In particular this is assumed in ConnectionCFEFnorm *)
fromCF[expr_] := FromCFTs4D[myToFormPairs[myExpandCFStructs[expr]] /. ConnectionCFEFnorm]


(* Kinematic prefactor of four-point functions *)
kinPref[\[CapitalDelta]1_, \[CapitalDelta]2_, \[CapitalDelta]3_, \[CapitalDelta]4_]:= 1/(sq[x[1,2]]^((\[CapitalDelta]1+\[CapitalDelta]2)/2) sq[x[3,4]]^((\[CapitalDelta]3+\[CapitalDelta]4)/2)) (sq[x[2,4]]/sq[x[1,4]])^((\[CapitalDelta]1-\[CapitalDelta]2)/2) (sq[x[1,4]]/sq[x[1,3]])^((\[CapitalDelta]3-\[CapitalDelta]4)/2);
kinPref[\[CapitalDelta]llb1_, \[CapitalDelta]llb2_, \[CapitalDelta]llb3_, \[CapitalDelta]llb4_]:= Module[{\[CapitalDelta]1,l1,lb1,\[CapitalDelta]2,l2,lb2,\[CapitalDelta]3,l3,lb3,\[CapitalDelta]4,l4,lb4,\[Kappa]1,\[Kappa]2,\[Kappa]3,\[Kappa]4},
	{\[CapitalDelta]1,l1,lb1} = If[Head[\[CapitalDelta]llb1]===List, \[CapitalDelta]llb1, {\[CapitalDelta]llb1,0,0}];
	{\[CapitalDelta]2,l2,lb2} = If[Head[\[CapitalDelta]llb2]===List, \[CapitalDelta]llb2, {\[CapitalDelta]llb2,0,0}];
	{\[CapitalDelta]3,l3,lb3} = If[Head[\[CapitalDelta]llb3]===List, \[CapitalDelta]llb3, {\[CapitalDelta]llb3,0,0}];
	{\[CapitalDelta]4,l4,lb4} = If[Head[\[CapitalDelta]llb4]===List, \[CapitalDelta]llb4, {\[CapitalDelta]llb4,0,0}];
	\[Kappa]1=\[CapitalDelta]1+1/2(l1+lb1);
	\[Kappa]2=\[CapitalDelta]2+1/2(l2+lb2);
	\[Kappa]3=\[CapitalDelta]3+1/2(l3+lb3);
	\[Kappa]4=\[CapitalDelta]4+1/2(l4+lb4);
	1/(sq[x[1,2]]^((\[Kappa]1+\[Kappa]2)/2) sq[x[3,4]]^((\[Kappa]3+\[Kappa]4)/2)) (sq[x[2,4]]/sq[x[1,4]])^((\[Kappa]1-\[Kappa]2)/2) (sq[x[1,4]]/sq[x[1,3]])^((\[Kappa]3-\[Kappa]4)/2)
];


(* ::Section::Closed:: *)
(*End of package*)


EndPackage[];
