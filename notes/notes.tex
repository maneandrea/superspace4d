\documentclass[11pt,a4paper,oneside]{article}

%Call my macro
\usepackage{macros}
\usepackage{tensor}

\newcommand{\xib}{\bar{\xi}}


\draftmode
\title{\textbf{Notes on $\boldsymbol{\CN=2}$ superspace}}

\date{}\author{}


\begin{document}

\maketitlesmall

In these notes we explain the various structures of the package \texttt{Superspace4d} and their properties.

The variables that are allowed in this package include positions, polarizations and Grassmann variables. They are all associated to a point in the $n$-point correlator,\footnote{In the reduced three-point function the variables are $X_i$, $\Theta_i$ and $\Thetab_i$. These must be denoted in the same way as $x_i$, $\theta_i$ and $\thetab_i$.} which is going to be denoted as $i$ in these notes. All other indices are suppressed because they are going to be contracted with the relevant polarizations. The variables are given by
\begin{enumerate}
\item \texttt{\texteta[\textit{i}]} $= \eta_i^\alpha$
\item \texttt{\texteta{b}[\textit{i}]} $= \etab_i^\alphad$
\item \texttt{\textxi[\textit{i}]} $= \xi_i^a$
\item \texttt{\texttheta[\textit{i}]} $= \theta_i^\alpha$ or $\theta_i^{a\alpha}$
\item \texttt{\texttheta{b}[\textit{i}]} $= \thetab_i^\alphad$ or $\thetab_i^{a\alphad}$
\item \texttt{x[\textit{i}]} $= (\rmx_i)_{\alpha\alphad}$ or $ (\tilde\rmx_i)^{\alphad\alpha}$, moreover \texttt{x[\textit{i},\textit{j}]} $ = \rmx_i-\rmx_j$
\end{enumerate}
The variables $\eta$ and $\etab$ are commuting spinor polarizations for the spin indices. The variable $\xi^a$ is a commuting spinor for the $\mathrm{SU}(2)$ R-symmetry index. The variables $\theta$ and $\thetab$ are the Grassmann variables. According to the context they can have an R-symmetry index ($\CN=2$ superspace) or not ($\CN=1$ superspace or $\CN=2$ analytic superspace). Finally $\rmx_{\alpha\alphad} = \sigma^\mu_{\alpha\alphad} x_\mu$ or $\tilde\rmx^{\alphad\alpha} = \sigmab^{\mu\alphad\alpha} x_\mu$ is the position variable. In what follows, if any of these variables is denoted in a slanted font without the brackets, possibly with a subscript, it means that it is a placeholder for an arbitrary point label. In this case the subscript is simply there to denote different variables of the same type unambiguously.


We have the following type of structures:
\begin{enumerate}
\item Chains terminating with spinor polarizations or with $\CN=1$ Grassmann variables
\begin{mathematica}
c[⟨\textit{\texteta$_{\text{1}}$}⟩,⟨\textit{x$_{\text{1}}$}⟩,...,⟨\textit{x$_{\text{2n}}$}⟩,⟨\textit{\texteta$_{\text{2}}$}⟩] = ⟨$\eta_1^\alpha \, (\rmx_1\cdots \tilde\rmx_{2n})\indices{_\alpha^\beta}\,\eta_{2\beta}$⟩;

c[⟨\textit{\texteta}⟩,⟨\textit{x$_{\text{1}}$}⟩,...,⟨\textit{x$_{\text{2n+1}}$}⟩,⟨\textit{\texteta{b}}⟩] = ⟨$\eta^\alpha\, (\rmx_1\cdots \rmx_{2n+1})\indices{_\alpha_\alphad}\, \etab^\alphad$⟩;

c[⟨\textit{\texteta{b}$_{\text{1}}$}⟩,⟨\textit{x$_{\text{1}}$}⟩,...,⟨\textit{x$_{\text{2n}}$}⟩,⟨\textit{\texteta{b}$_{\text{2}}$}⟩] = ⟨$\etab_{1\alphad} \, (\tilde\rmx_1\cdots \rmx_{2n})\indices{^\alphad_\betad}\, \etab_2^\betad$⟩;
\end{mathematica}
We use precisely these conventions for the position of the indices of the $\eta$'s. There are analogous definitions for $\eta^\alpha\to\theta^\alpha$ and $\etab^\alphad\to\thetab^\alphad$.

\item Chains containing R-symmetry indices
\begin{mathematica}
rc[⟨\textit{\textxi$_{\text{1}}$}⟩,⟨\textit{\texttheta$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n}}$}⟩,⟨\textit{\texttheta$_{\text{2}}$}⟩,⟨\textit{\textxi$_{\text{2}}$}⟩] = ⟨$\xi_1^a\,\theta_{1a}^\alpha \, (\rmz_1\cdots \tilde\rmz_{2n})\indices{_\alpha^\beta}\,\theta^b_{2\beta}\,\xi_{2b}$⟩;

rc[⟨\textit{\textxi$_{\text{1}}$}⟩,⟨\textit{\texttheta{b}$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n}}$}⟩,⟨\textit{\texttheta{b}$_{\text{2}}$}⟩,⟨\textit{\textxi$_{\text{2}}$}⟩] = ⟨$\xi_1^a\,\thetab_{1a\alphad}\, (\tilde\rmz_1\cdots \rmz_{2n})\indices{^\alphad_\betad}\,\thetab^{b\betad}_2\,\xi_{2b}$⟩;

rc[⟨\textit{\textxi$_{\text{1}}$}⟩,⟨\textit{\texttheta$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n+1}}$}⟩,⟨\textit{\texttheta{b}$_{\text{2}}$}⟩,⟨\textit{\textxi$_{\text{2}}$}⟩] = ⟨$\xi_1^a\,\theta_{1a}^\alpha \, (\rmz_1\cdots \rmz_{2n+1})\indices{_\alpha_\betad}\,\thetab^{b\betad}_2\,\xi_{2b}$⟩;
\end{mathematica}
Here we used the placeholder $\rmz_{\alpha\alphad}$ to denote either a position $\rmx_{\alpha\alphad}$ or the combination $\theta_{i\alpha}^a\thetab_{j\lsp a\alphad}$. Another possibility is to replace $\rmz_{\alpha\alphad}\tilde\rmz^{\alphad\beta}$ with $\theta_{i\alpha}^a\theta_{ja}^\beta$ or $\tilde\rmz^{\alphad\alpha}\rmz_{\alpha\betad}$ with $\thetab_i^{a\alphad}\thetab_{j\lsp a\betad}$. The convention for the R-symmetry indices is that they are always contracted upper to lower.


\item Trace with position variables only
\begin{mathematica}
t[⟨\textit{x$_{\text{1}}$}⟩,...,⟨\textit{x$_{\text{2n}}$}⟩] = ⟨$\tr(\rmx_1\cdots \tilde\rmx_{2n})$⟩;
\end{mathematica}
We use the convention where the first matrix is always with lower indices.

\item Trace with R-symmetry indices
\begin{mathematica}
rt[⟨\textit{\texttheta$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n}}$}⟩,⟨\textit{\texttheta$_{\text{2}}$}⟩] = ⟨$\theta_{1a}^\alpha \, (\rmz_1\cdots \tilde\rmz_{2n})\indices{_\alpha^\beta}\,\theta^a_{2\beta}$⟩;

rt[⟨\textit{\texttheta{b}$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n}}$}⟩,⟨\textit{\texttheta{b}$_{\text{2}}$}⟩] = ⟨$\thetab_{1a\alphad}\, (\tilde\rmz_1\cdots \rmz_{2n})\indices{^\alphad_\betad}\,\thetab^{a\betad}_2$⟩;

rt[⟨\textit{\texttheta$_{\text{1}}$}⟩,⟨\textit{z$_{\text{1}}$}⟩,...,⟨\textit{z$_{\text{2n+1}}$}⟩,⟨\textit{\texttheta{b}$_{\text{2}}$}⟩] = ⟨$\theta_{1a}^\alpha \, (\rmz_1\cdots \rmz_{2n+1})\indices{_\alpha_\betad}\,\thetab^{a\betad}_2$⟩;
\end{mathematica}
Here we used the placeholder $\rmz_{\alpha\alphad}$ to denote either a position $\rmx_{\alpha\alphad}$ or the combination $\theta_{i\alpha}^a\thetab_{j\lsp a\alphad}$. Another possibility is to replace $\rmz_{\alpha\alphad}\tilde\rmz^{\alphad\beta}$ with $\theta_{i\alpha}^a\theta_{ja}^\beta$ or $\tilde\rmz^{\alphad\alpha}\rmz_{\alpha\betad}$ with $\thetab_i^{a\alphad}\thetab_{j\lsp a\betad}$. The convention for the R-symmetry indices is that they are always contracted upper to lower, furthermore the first is lower and the last is upper.

\item Dot product
\begin{mathematica}
d[⟨\textit{x$_{\text{1}}$}⟩,⟨\textit{x$_{\text{2}}$}⟩] = ⟨$x_1^\mu\, x_{2\mu}$⟩;

sq[⟨\textit{x}⟩] = d[⟨\textit{x}⟩,⟨\textit{x}⟩] = ⟨$x^2$⟩;
\end{mathematica}
This is of course redundant as $\tr(\rmx_1\rmx_2) = -2\lsp x_1\cdot x_2$.

\item Levi-Civita tensor
\begin{mathematica}
e[⟨\textit{x$_{\text{1}}$}⟩,⟨\textit{x$_{\text{2}}$}⟩,⟨\textit{x$_{\text{3}}$}⟩,⟨\textit{x$_{\text{4}}$}⟩] = ⟨$\epsilon^{\mu\nu\rho\lambda}\, x_{1\mu}\, x_{2\nu}\, x_{3\rho}\, x_{4\lambda}$⟩;
\end{mathematica}
with $\epsilon^{0123}=1$. This is also redundant because $\epsilon\cdot x_1 x_2 x_3 x_4 = \frac{i}2 \tr(\rmx_1 \tilde\rmx_2 \rmx_3 \tilde\rmx_4) + \cdots$ where the dots are a combination of scalar products $x_i\cdot x_j$.

\end{enumerate}


\end{document}
