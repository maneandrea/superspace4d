Superspace 4d
====

Mathematica tool to perform computations in four dimensional N=1 or N=2 superspace using the index-free formalism.

Getting started
---

###  Requisites

This package has been developed and tested on *Mathematica 13*.

### Installing

The package can be loaded in a Mathematica session by runninng
```
<<Superspace4d` 
```

after putting the file *Superspace4d.wl* into a folder contained in `$Path`. Otherwise it is also possible to put Superspace4d.wl in any folder and load it with
```
Get["<path-to-folder>/Superspace4d.wl"];
```

Documentation
---

The documentation is still work in progress. Here is a high-level overview.

### Scope

This package can be used to make computations in superspace using the index-free formalism. The idea is that *all* indices will be contracted with appropriate polarizations so that all expressions are polynomials without open indices.

The constructs defined here can be used for all computations in four dimensional $\mathcal{N} = 1$ superspace.

This package can also be used for $\mathcal{N} = 2$ *analytic* superspace. Therefore, half-BPS n-point functions can be expressed in this package. Some modern (and simplified) overview of analytic superspace can be found here: [2209.01204](https://arxiv.org/abs/2209.01204).

### Constructs

There are special symbols for the various objects. η and ηb are left and right Weyl commuting spinors, used to contract indices. 
θ and θb are left and right Weyl anticommuting spinors, which represent the Grassman variables in superspace. x is the Lorentz position of the vector, which could be either in its vector or its matrix form.

Each of these symbols has a *point* `θ[1]`, `η[2]`, `x[3,4]`, ... which are simply labels. They could be labels in a four-point function, or simply additional labels for placeholder variables.

A *chain* `c[...]` is a product of x matrices contracted with spinors. Depending on the parity of the matrices in between, the start and end spinor might be of the same or opposite chirality.

By convention, if the chain starts with a right (i.e. unbarred) spinor, x has *lower* $\alpha$, $\dot{\alpha}$ indices when in an odd position and *upper* $\dot{\alpha}$, $\alpha$ indices when in an even position. If the chain instead starts with a barred spinor it's the other way around.

Chains with no x's are like Wess & Bagger: undotted indices are contracted up to down ↘ and dotted indices down to up ↗.

A *trace* `t[...]` is, like the name says, a trace of spinor matrices. By convention, the first matrix has lower $\alpha$, $\dot{\alpha}$ indices and then they alternate just like in a chain. They must have an even number of matrices and are invariant under cyclic shifts that are multiples of two.

A *dot* `d[_,_]` is a Lorentz contraction between two vectors using the metric $g_{\mu\nu}$.

An *epsilon* `e[_,_,_,_]` is a full contraction of the epsilon tensor with lower indices with four vectors with upper indices, using $\epsilon^{0123} = 1$.

### Reduction and expression manipulation

There are various functions to reduce a chain into smaller chains. These functions leverage the fact that contiguous x matrices that are equal reduce to a Lorentz product $x^2$, that matching θ's in the same product reduce to a $\theta^2$ and that products of two epsilons can be recast as dot products. All these reduction rules can be applied using the function `allReduce[<expr>]`.

Sometimes one needs to raise a term to some power or to do a Taylor expansion. If the term involves fermionic symbols, the power expands to some finite number of terms. The function that deals with this is `fPower[<expr>, <power>, <nmax>]`. If the power is some symbol or some real number, the parameter `<nmax>` determines how much to push the Taylor expansion of `(bosonic+fermionic)^pow`.


### Differential operators

The core strength of the package is the possibility to define arbitrarily complex differential operators. Since this entire package deals with fully contracted expressions, there is no way to define a term with a free $\alpha$ or $\dot{\alpha}$ index. We have to, instead, introduce placeholder polarizations η and ηb and take derivatives with respect to them. Schematically, if we have some differential operator $D_\alpha$ and some expression $X^\alpha$, then we would compute $D_\alpha X^\alpha$ by $\partial_\eta D \,(\eta X)$.

Some differential operators are predefined. There are all variations with two spinors ηdη corresponds to $\eta\partial_\eta$, ηdθ to $\eta\partial\theta$, xdx to $x^\mu \partial_\mu$ etc...

There are also composite, or higher order, differential operator, like dηdη, which is $\partial_\eta\partial_\eta$. The only tricky thing to keep in mind about spinor derivative is that their signs work differently from the spinor themselves: $\eta^\alpha = \epsilon^{\alpha\beta}\eta_\beta$ while $\partial_{\eta_\alpha} = \epsilon^{\beta\alpha}\partial_{\eta^\beta}$. There is also the Laplacian operator dxdx.

Additionally, there are predefined covariant derivatives $D$, $\overline{D}$, $Q$ and $\overline{Q}$ defined as opD, opDb, opQ, opQb. If they are called with a spinor called η or ηb, it's like having a derivative with a free index (e.g. `opD[i,η[j]][<expr>]` = $\eta_iD_i\langle\mathrm{expr}\rangle$), while if they are contracted with a spinor called dη or dηb, it's like contracting that spinor index as well (e.g. `opD[i,dη[j]][<expr>]` = $\partial_{\eta_i}D_i\langle\mathrm{expr}\rangle$). The difference between D and Q is whether they act as left invariant vectors fields or right invariant vector fields in superspace. Appendix A of [1910.12869](https://arxiv.org/abs/1910.12869) has all the conventions.

Finally, you can create your own differential operators! You can look at the definitions of the aforementioned D's and Q's in the source code to understand well the syntax. But, in short, you have to just write a term (which could be a chain, a dot, a trace, or a produce thereof) containing one or more symbols that start with `d`. For example it could contain dη or dθb or dx. The package will automatically recognize all terms with a d and turn them into derivatives. You can define such an operator for a term `<term>` as `composideD[<term>]` and then you can apply that to expressions `compositeD[<term>][<expr>]`.

### Comparison

Another strong feature of this package is the ability to compare complicated expressions and set them equal. It is very difficuly to compare two expressions because of the *Shouten identities*. There is no canonical form to put an expression in, so it's difficult to compare some arbitrary expression to zero.

Shouten identities are consequences of $\epsilon^{(\alpha\beta}\epsilon^{\gamma)\delta} = 0$ where the parentheses indicate a sum over cyclic permutations.

This package replaces spinors and vectors with random rational numbers and compare the resulting number to zero. Since these are fully contracted expressions, the result is always a number. Since the replacements are rational, there are no floating point issues.

You can use the function `compare`  as follows `compare[<expr>, <vars>]`. The variables `<vars>` are the coefficients on which the expression `<expr>` depends. The result is a replacement list for the variables in `<vars>` that make the expression zero. To compare two expressions X and Y simply compute
`compare[X - a B, {a}]` and see if `{a -> 1}` is a solution.

The most common usage of this feature is for imposing symmetry or conservation conditions on expressions. If a three-point function depends on three variables a, b and c and it needs to be conserved, one can do
```
compare[opD[1, dη[1]][<expr>], {a, b, c}]
```
The result will be a list of replacement rules that defines the linear relations among `a`, `b` and `c`.

### Correlators

This package leverates [CFTs4D](https://gitlab.com/bootstrapcollaboration/CFTs4D) to generate three-point functions but can generate correlators for the $\mathcal{N}=1$ supersymmetric case as well.

For the non supersymmetric case, a wrapper of the CFTs4D function is 
```
nonSUSY3pf[{Δ1,Δ2,Δ3},{{jl1,jb1},{j2,jb2},{j3,jb3}}]
```
Where the labels `j1`, `jb1`, ... are the spinor Dynkin labels (they are integer, so vectors are $(1,1)$, Weyl spinors $(1,0)$, $(0,1)$, etc..).

For supersymmetric three-point functions it is convenient to factor out a kinematical prefactor as in [hep-th/9808041](https://arxiv.org/abs/hep-th/9808041). To re-expand the full three-point function by reapplying the prefactor you can use the function 
```
applyPrefactor[{Δ1,j1,jb1}, {Δ2,j2,jb2}, <three-point-function>]
```
This is useful because CFTs4D gives the three-point function as a function of all three points, while it's easier to work in superspace with the single point variables $X$, $\Theta$ and $\overline{\Theta}$. So if we want to expand a three-point function into its superdescendant orders, we have to apply the prefactor.

Note that, since $X$, $\Theta$ and $\overline{\Theta}$ behave like the superpace variables $x$, $\theta$ and $\bar{\theta}$, you can work with them in this package by defining them just like other variables e.g. `x[3]`, `θ[3]`, `θb[3]`. In fact, the function `applyPrefactor` expects them to be called either `x[1]` or `x[2]` or `x[3]` and the *point* represents the point that is "left out" from the prefactor. The difference is just a cyclic permutation, as discussed around eq (2.51) of [hep-th/9808041](https://arxiv.org/abs/hep-th/9808041).

---

For any questions, feel free to contact me at
andrea **[dot]** manenti **[at]** yahoo **[dot]** com
